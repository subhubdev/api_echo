<?php
require_once('/home/subhub/core/EVO-4.0.0/includes/neuron/adodb/adodb.inc.php');
class SiteDatabase {

        private $instance = null;
        private $dbconnect;
        private $message; 
        private $db;

        public function __get($db) {
            if (property_exists($this, $db)) {
                return $this->$db;
            }
        }
    
        public function __set($db, $value) {
            if (property_exists($this, $db)) {
                $this->$db = $value;
            }
            return $this;
        }
        
        public function dbConnect() {
            if (property_exists($this, 'dbconnect')) { 
                try {
                    $config = parse_ini_file('app.config.ini');
                    $this->dbconnect = NewADOConnection('mysqli');
                    if (!$this->dbconnect) die("Connection failed");   
                    $this->dbconnect->SetFetchMode(ADODB_FETCH_ASSOC);
                    $this->dbconnect->Connect($config['hostlive'], $config['username'], $config['password'], $this->db);
                    // $this->dbconnect->Connect($config['host'], $config['usernametest'], $config['passwordtest'], $this->db) // Local Test;
                 }
                 catch (Exception $ex) {
                     return $this->message = $ex->getMessage();
                 }
                 return $this->dbconnect;
            }
        }
}

?>