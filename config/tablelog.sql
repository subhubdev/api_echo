DROP TABLE IF EXISTS `gsass_logs`;
CREATE TABLE IF NOT EXISTS `gsass_logs` (
  `iid` int(11) NOT NULL AUTO_INCREMENT,
  `logs` text,
  `event` VARCHAR(50) NOT NULL DEFAULT '',
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  PRIMARY KEY (`iid`),
  INDEX `event` (`event`),
  INDEX `dateevent` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;