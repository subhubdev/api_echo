Global Variable

CORE_ENV 
@value = PRODUCTION = api STAGING = api_staging

API_ROOT
@value = /var/www/html/sites/'.CORE_ENV.'/firebase-evolistener/

CORE_VERSION
@value = EVO-4.0.0

NEURON_ROOT 
@value = /home/subhub/core/'.CORE_VERSION.'/'


API Parameters
['id', 'uid', 'gsass', 'homepagetype', 'groups'];

/**
 * id, gsass site db name
 * uid site admin email
 */

firebasedatatype
@value  = ['general_settings', 'sections', 'top_navigation', 'buttom_navigation', 'sidebar'];

Mailchimp
Live Editor usage: http://site.local/firebase-evolistener?id=sh_db&uid=siteadmin@subhub.com
    Get mailchimp from evo

Generate Sass
Live Editor usage: http://site.local/firebase-evolistener?gsass=sh_db&uid=siteadmin@subhub.com&homepagetype=public-homepages 
    Generate Sass File Path = design/templates/Business/sass/sections  || design/templates/Business/sass/sidebar

Members Group
Live Editor usage: http://site.local/firebase-evolistener?groups=sh_db&uid=siteadmin@subhub.com
    Get members group from evo

Save Firebase Data in SQL
Live Editor usage: http://site.local/firebase-evolistener?dataid=sh_db&uid=siteadmin@subhub.com.com&firebasedatatype=general_settings
    Save Firebase Data in EVO SQL

/*
 * Live Editor: api.services.ts
*/
