<?php
// error_reporting(E_ALL);

require_once(API_ROOT. 'config/Database.php');
require_once(API_ROOT. 'model/UserAccount.php');
require_once(API_ROOT. 'model/Logs.php');
require_once(API_ROOT. 'model/Notification.php');
require_once(API_ROOT. 'model/Settings.php');
require_once(API_ROOT. 'model/SaveFirebaseData.php');
require_once(API_ROOT. 'model/GenerateSassLanding.php');

$database = new SiteDatabase();

$database->db = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();

$db = $database->dbconnect();

$post_response = array();

$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);

global $settings;
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'save firebase to evo';
    $apiLogs->saveLogs($userAccount->logs);
    $post_response_item = array(
        "id" => '1000', //Response ID == 1000 -> Invalid Admin email
        "name" => "Invalid Admin email. (save firebase to evo)",
        "logs" => "$userAccount->logs"
    );

    // send to slack
    $slack = new SlackNotification();
    $slackMsg = sprintf("<{$settings->secure_url}>\n%s \n%s ", 
    "Error Type: Invalid Admin email (save firebase to evo)", "Logs: {$userAccount->logs}");
    $slack->send($slackMsg);

    array_push($post_response, $post_response_item);
    echo json_encode($post_response);
    die();
} 

$errorType = '';
$response ='';

$lpEvents = $_GET['landing-page-events'];
$response = $lpEvents;
$fb = new FireBaseData($db);
$landingPage = new GenerateSassLanding($db);

switch ($lpEvents) {
    case 'landing-page-save' :
        $lpData = $landingPage->getMainLandingPage('landing_page');
        if (!empty($lpData)) {
            $fb->clearSeoMeta();
            foreach($lpData as $lpVal) {
                $result = $fb->saveLandingPage($lpVal);
            }
        }
    break;
}

if(!empty($fb->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = $lpEvents;
    $apiLogs->saveLogs($fb->logs);

     // send to slack
     $slack = new SlackNotification();
     $slack->sendErrorLogs($fb->logs, $response, $settings->secure_url);
}

echo json_encode($result);