<?php
// error_reporting(E_ALL);

use function GuzzleHttp\json_decode;

require_once(API_ROOT. 'config/Database.php');
require_once(API_ROOT. 'model/UserAccount.php');
require_once(API_ROOT. 'model/Logs.php');
require_once(API_ROOT. 'model/Notification.php');
require_once(API_ROOT. 'model/DripContentManager.php');
require_once(API_ROOT. 'model/Settings.php');
require_once(API_ROOT. 'model/FirebaseFunctions.php');

$database = new SiteDatabase();

$database->db = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();

$db = $database->dbconnect();

$post_response = array();
global $settings;

$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'drip content manager';
    $apiLogs->saveLogs($userAccount->logs);
    $post_response_item = array(
        "id" => '1000', //Response ID == 1000 -> Invalid Admin email
        "name" => "Invalid Admin email. (drip content manager)",
        "logs" => "$userAccount->logs"
    );

    // send to slack
    $slack = new SlackNotification();
    $slackMsg = sprintf("<{$settings->secure_url}>\n%s \n%s ", 
    "Error Type: Invalid Admin email (drip content manager) File: ".__FILE__ , "Logs: {$userAccount->logs}");
    $slack->send($slackMsg);

    array_push($post_response, $post_response_item);
    echo json_encode($post_response);
    die();
} 

$errorType = '';
$drip = new DripContentManager($db);
// $fbf = new FirebaseFunctions();
$planId = filter_var(trim($_GET['paymentplanid']), FILTER_SANITIZE_NUMBER_INT);
$response ='';
// get All paymentplan
if(filter_var(trim($_GET['paymentplan'])) == 'all') {
    $result = $drip->getPaymentPlans();
}
// get all article by planid
if(isset($_GET['article_drip']) && isset($_GET['paymentplanid'])) {
    $result = $drip->getArticlesByPlanId($planId);
}

//Post Drip content
if(filter_var(trim($_GET['published_drip']))) {

    $post_data = json_decode(file_get_contents("php://input"), true);
    $result = $drip->saveDripContent($post_data);
    $response = 'published_drip';
    // $fb_data = $fbf->getDripContenSections('members');
    // $drip->saveDripContent($fb_data);
}


// Update course content
if(filter_var(trim($_GET['delete_course_content']))) {
    // $contentId = filter_var(trim($_GET['course_content_id']), FILTER_SANITIZE_NUMBER_INT);
    // $drip->deleteCourseContentByContentIdAndPlanId($contentId);
    // $drip->unlinkCourseContentInDripSchedule($contentId, $planId);
}


if(!empty($drip->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'dripcontent';
    $apiLogs->saveLogs($drip->logs);

     // send to slack
     $slack = new SlackNotification();
     $slack->sendErrorLogs($drip->logs, $response, $settings->secure_url);
}

echo json_encode($result);