<?php
// error_reporting(E_ALL);
include_once API_ROOT. 'config/Database.php';
include_once API_ROOT. 'model/GenerateSassLanding.php';
include_once API_ROOT. 'model/UserAccount.php';
include_once API_ROOT. 'model/Logs.php';
include_once API_ROOT. 'model/Settings.php';
include_once API_ROOT. 'model/Notification.php';
include_once API_ROOT. 'model/SaveFirebaseData.php';
include_once API_ROOT. 'model/TopNavAndFooterBlock.php';

// Get[gsass] Live editor Collection Name 
$database = new SiteDatabase();

$database->db = isset($_GET['gsass']) ? filter_var(trim($_GET['gsass']), FILTER_SANITIZE_STRING) : die();
$homepage = isset($_GET['homepagetype']) ? filter_var(trim($_GET['homepagetype']), FILTER_SANITIZE_STRING) : die();

$db = $database->dbconnect();

$post_response = array();

$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);

global $settings;

$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'generate_saas_landing';
    $apiLogs->saveLogs($userAccount->logs);
    $post_response_item = array(
        "id" => '1000', //Response ID == 1000 -> Invalid Admin email
        "name" => "Invalid Admin email.",
        "logs" => "$userAccount->logs"
    );
    array_push($post_response, $post_response_item);
    echo json_encode($post_response);
    die();
} 

$generate = new GenerateSassLanding($db);
$generate->homepage = $homepage;
$result = $generate->generateSassLanding();

if(!empty($generate->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'generate_saas_landing';
    $apiLogs->saveLogs($generate->logs);
}


if ($result) {

    if ( $result == 1 ) {
        $post_response_item = array(
            "id" => $result, //Response ID == 1 -> SUCCESSFUL in SASS compilation
            "name" => "Landing Page SASS successfully compiled."
        );

    } else if ( $result == 3 ) {
        $post_response_item = array(
            "id" => $result, //Response ID == 3 -> UNSUCCESSFUL in SASS compilation
            "name" => "Error in Landing Page SASS compiler."
        );

    }
    array_push($post_response, $post_response_item);

}
else {
    $post_response_item = array(
        "id" => $result, //Response ID == 0 -> No Firebase Data was received
        "name" => "Error in getting firebase data."
    );
    array_push($post_response, $post_response_item);
}

if(!empty($generate->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'generate_saas_landing';
    $apiLogs->saveLogs($generate->logs);
    // send to slack
    $slack = new SlackNotification();
    $slack->sendErrorLogs($generate->logs, $post_response[0]['name'], $settings->secure_url);
}
echo json_encode($post_response);