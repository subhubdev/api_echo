<?php
include_once API_ROOT. 'config/Database.php';
include_once API_ROOT. 'model/NeuronGoups.php';
include_once API_ROOT. 'model/UserAccount.php';
include_once API_ROOT. 'model/Logs.php';

$database = new SiteDatabase();
// Get[id] Live editor Collection Name 
$database->db = isset($_GET['groups']) ? filter_var(trim($_GET['groups']), FILTER_SANITIZE_STRING) : die();
//add validate length and strpos sh_
$db = $database->dbconnect();

$userAccount = new UserAccount($db);
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'neuron_group';
    $apiLogs->saveLogs($userAccount->logs);
    die();
}

$group = new NeuronGroups($db);

// Get Mailchimp List
$result = [];
$result = $group->getGroups();

if(!empty($group->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'neuron_group';
    $apiLogs->saveLogs($group->logs);
}

echo json_encode($result);

// if(!empty($result)) {
//     echo json_encode($result);
// }
// else {
//     echo json_encode($result);
    // $group_error = array();
    // $group_error_item = array(
    //     "id"=> "0",
    //     "name" => "No result"
    // );
    // array_push($group_error, $group_error_item);
    // echo json_encode($group_error);
// }
?>