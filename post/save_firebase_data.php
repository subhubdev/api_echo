<?php

require_once(API_ROOT. 'config/Database.php');
require_once(API_ROOT. 'model/SaveFirebaseData.php');
require_once(API_ROOT. 'model/UserAccount.php');
require_once(API_ROOT. 'model/Logs.php');
require_once(API_ROOT. 'model/FirebaseFunctions.php');
require_once(API_ROOT. 'model/Notification.php');
require_once(API_ROOT. 'model/Settings.php');
require_once(API_ROOT. 'model/TopNavAndFooterBlock.php');

$database = new SiteDatabase();
// Get[id] Live editor Collection Name 
$database->db = filter_var(trim($_GET['dataid']), FILTER_SANITIZE_STRING);
//add validate length and strpos sh_
$firebaseType = filter_var(trim($_GET['firebasedatatype']), FILTER_SANITIZE_STRING);

$db = $database->dbconnect();

global $settings;

$settings = new EvoApiSettings($db);

$userAccount = new UserAccount($db);
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'save_firebase_data';
    $apiLogs->saveLogs($userAccount->logs);
    die();
}

$fbf = new FirebaseFunctions();
$fb = new FireBaseData($db);

$fb_data = '';
// sections, top_navigation,  buttom_navigation, sidebar, general_settings
switch($firebaseType) {
    case 'general_settings':
        $fb_data = $fbf->getGeneralSettings();
    break;

    case 'top_navigation_footer':
        $topAndFooter = new ApiTopNavAndFooter();
        $topAndFooter->homepageType = 'public';
        $fb_data['public'] = $topAndFooter->init();
        $topAndFooter->homepageType = 'members';
        $fb_data['members'] = $topAndFooter->init();
    break;

    case 'sections' :
        // $fb_data = $fbf->getHomepageSections();
    break;

    case 'sidebar' :
    break;
}

if (!empty($fb_data)) {
    $fb->type = $firebaseType;
    $result = $fb->saveFirebaseDataToSql($fb_data);
}

if(!empty($fb->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'save_firebase_data';
    $apiLogs->saveLogs($fb->logs);

    $slack = new SlackNotification();
    $slack->sendErrorLogs($fb->logs, 'save_firebase_data', $settings->secure_url);
}


if($result) {
    echo json_encode($result);
}
else {
    $fb_error = array();
    $fb_error_item = array(
        "id"=> "0",
        "name" => "No result"
    );
    array_push($fb_error, $fb_error_item);
    echo json_encode($fb_error);
}
?>