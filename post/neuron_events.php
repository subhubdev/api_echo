<?php
// error_reporting(E_ALL);
include_once API_ROOT. 'config/Database.php';
include_once API_ROOT. 'model/UserAccount.php';
include_once API_ROOT. 'model/Logs.php';
include_once API_ROOT. 'model/Notification.php';
include_once API_ROOT. 'model/ApiEvents.php';
include_once API_ROOT. 'model/Settings.php';

// Get[siteid] Live editor Collection Name 
$database = new SiteDatabase();

$database->db = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();

$db = $database->dbconnect();

$post_response = array();
global $settings;

$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'neuron_events';
    $apiLogs->saveLogs($userAccount->logs);
    $post_response_item = array(
        "id" => '1000', //Response ID == 1000 -> Invalid Admin email
        "name" => "Invalid Admin email.",
        "logs" => "$userAccount->logs"
    );

    // send to slack
    $slack = new SlackNotification();
    $slackMsg = sprintf("<{$settings->secure_url}>\n%s \n%s ", 
    "Error Type: Invalid Admin email", "Logs: {$userAccount->logs}");
    $slack->send($slackMsg);

    array_push($post_response, $post_response_item);
    echo json_encode($post_response);
    die();
} 

$errorType = '';
$events = new ApiEvents($db);
$result = $events->getAllEvents();

// if(!empty($events->logs)) {
//     $apiLogs = new ApiLogs($db);
//     $apilogs->event = 'neuron_events';
//     $apiLogs->saveLogs($events->logs);
// }

echo json_encode($result);