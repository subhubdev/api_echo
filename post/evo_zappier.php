<?php
// error_reporting(E_ALL);

use function GuzzleHttp\json_decode;

require_once(API_ROOT. 'config/Database.php');
require_once(API_ROOT. 'model/UserAccount.php');
require_once(API_ROOT. 'model/Logs.php');
require_once(API_ROOT. 'model/Notification.php');
require_once(API_ROOT. 'model/DripContentManager.php');
require_once(API_ROOT. 'model/Settings.php');

$database = new SiteDatabase();

$database->db = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();

$db = $database->dbconnect();

$post_response = array();

$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);

global $settings;
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'drip content manager';
    $apiLogs->saveLogs($userAccount->logs);
    $post_response_item = array(
        "id" => '1000', //Response ID == 1000 -> Invalid Admin email
        "name" => "Invalid Admin email. (drip content manager evo zap)",
        "logs" => "$userAccount->logs"
    );

    // send to slack
    $slack = new SlackNotification();
    $slackMsg = sprintf("<{$settings->secure_url}>\n%s \n%s ", 
    "Error Type: Invalid Admin email (drip content manager) File: ".__FILE__ , "Logs: {$userAccount->logs}");
    $slack->send($slackMsg);

    array_push($post_response, $post_response_item);
    echo json_encode($post_response);
    die();
} 

$errorType = '';
$drip = new DripContentManager($db);
$response ='';

//Add member courses from ZAP
// $post_data = json_decode(file_get_contents("php://input"), true);

$zapevents = $_GET['zapevents'];
$post_data['paymentplanid'] = $_GET['paymentplanid'];
$post_data['member_id'] = $_GET['member_id'];

switch ($zapevents) {
    case 'add-member-course' :
        $result = $drip->addDripContent($post_data);
        $response = 'add-member-course';
    break;
}


if(!empty($drip->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'zap-add-member-course';
    $apiLogs->saveLogs($drip->logs);

     // send to slack
     $slack = new SlackNotification();
     $slack->sendErrorLogs($drip->logs, $response, $settings->secure_url);
}

echo json_encode($result);