<?php
// error_reporting(E_ALL);
include_once API_ROOT. 'config/Database.php';
include_once API_ROOT. 'model/GenerateSass.php';
include_once API_ROOT. 'model/UserAccount.php';
include_once API_ROOT. 'model/Logs.php';
include_once API_ROOT. 'model/Settings.php';
include_once API_ROOT. 'model/Notification.php';
include_once API_ROOT. 'model/SaveFirebaseData.php';
include_once API_ROOT. 'model/TopNavAndFooterBlock.php';

// Get[gsass] Live editor Collection Name 
$database = new SiteDatabase();

$database->db = isset($_GET['gsass']) ? filter_var(trim($_GET['gsass']), FILTER_SANITIZE_STRING) : die();
$homepage = isset($_GET['homepagetype']) ? filter_var(trim($_GET['homepagetype']), FILTER_SANITIZE_STRING) : die();
$homepage = (strpos(strtolower($homepage), 'public') !== false) ? 'public' : (strpos(strtolower($homepage), 'members') !== false) ? : 'members';

$db = $database->dbconnect();

$post_response = array();
global $settings;

$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'generate_saas';
    $apiLogs->saveLogs($userAccount->logs);
    $post_response_item = array(
        "id" => '1000', //Response ID == 1000 -> Invalid Admin email
        "name" => "Invalid Admin email.",
        "logs" => "$userAccount->logs"
    );

    // send to slack
    $slack = new SlackNotification();
    $slackMsg = sprintf("<{$settings->secure_url}>\n%s \n%s ", 
    "Error Type: Invalid Admin email", "Logs: {$userAccount->logs}");
    $slack->send($slackMsg);

    array_push($post_response, $post_response_item);
    echo json_encode($post_response);
    die();
} 

$errorType = '';
$generate = new GenerateSass($db);
$generate->homepage = $homepage;
$generate->siteroot = strtolower(trim($settings->sitename));
$result = $generate->generateSass();

if ($result) {

    if ( $result == 1 ) {
        $post_response_item = array(
            "id" => $result, //Response ID == 1 -> SUCCESSFUL in SASS compilation
            "name" => "SASS successfully compiled."
        );

    } else if ( $result == 3 ) {
        $post_response_item = array(
            "id" => $result, //Response ID == 3 -> UNSUCCESSFUL in SASS compilation
            "name" => "Error in SASS compiler."
        );
    
    }
    array_push($post_response, $post_response_item);

    // Save Top ANd Footer Block in SQL
    $fb = new FireBaseData($db);
    $topAndFooter = new ApiTopNavAndFooter();
    $topAndFooter->homepageType = 'public';
    $fb_data['public'] = $topAndFooter->init();
    $topAndFooter->homepageType = 'members';
    $fb_data['members'] = $topAndFooter->init();
    if (!empty($fb_data)) {
        $firebaseType = 'top_navigation_footer';
        $fb->type = $firebaseType;
        $result = $fb->saveFirebaseDataToSql($fb_data);
    }
}
else {
    $post_response_item = array(
        "id" => $result, //Response ID == 0 -> No Firebase Data was received
        "name" => "Error in getting firebase data."
    );
    array_push($post_response, $post_response_item);
}

if(!empty($generate->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'generate_saas';
    $apiLogs->saveLogs($generate->logs);
    // send to slack
    $slack = new SlackNotification();
    $slack ->sendErrorLogs($generate->logs, $post_response[0]['name'], $settings->secure_url);
}

echo json_encode($post_response);