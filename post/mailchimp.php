<?php

include_once API_ROOT. 'config/Database.php';
include_once API_ROOT. 'model/PostMailchimp.php';
include_once API_ROOT. 'model/UserAccount.php';
include_once API_ROOT. 'model/Logs.php';
include_once API_ROOT. 'model/Notification.php';
include_once API_ROOT. 'model/Settings.php';

$database = new SiteDatabase();
// Get[id] Live editor Collection Name 
$database->db = isset($_GET['id']) ? filter_var(trim($_GET['id']), FILTER_SANITIZE_STRING) : die();
//add validate length and strpos sh_
$db = $database->dbconnect();

global $settings;
$settings = new EvoApiSettings($db);
$userAccount = new UserAccount($db);
$userExist = $userAccount->checkAdminEmail($_GET['uid']);
if (!$userExist) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'mailchimp_list';
    $apiLogs->saveLogs($userAccount->logs);

    // send to slack
    $slack = new SlackNotification();
    $slackMsg = sprintf("<{$settings->secure_url}>\n%s \n%s ", 
    "Error Type: Invalid Admin email", "Logs: {$userAccount->logs}");
    $slack->send($slackMsg);
    die();
}

$post = new PostMailchimp($db);

// Get Mailchimp List
$result = $post->getList();

if(!empty($post->logs)) {
    $apiLogs = new ApiLogs($db);
    $apilogs->event = 'mailchimp_list';
    $apiLogs->saveLogs($post->logs);
}

if($result) {
    echo json_encode($result);
}
else {
    $post_error = array();
    $post_error_item = array(
        "id"=> "0",
        "name" => "No result"
    );
    array_push($post_error, $post_error_item);
    echo json_encode($post_error);
}

?>