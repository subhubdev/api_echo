<?php
// error_reporting(E_ALL);
define('CORE_ENV', 'api'); //@value PRODUCTION = api STAGING = api_staging
define('API_ROOT','/var/www/html/sites/'.CORE_ENV.'/firebase-evolistener/');

define('CORE_VERSION', 'EVO-4.0.0');
define('VERSION_NUMBER', '4.0.0');
define('NEURON_ROOT','/home/subhub/core/'.CORE_VERSION.'/');

$origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : $_SERVER['HTTP_ORIGIN'];
$allowedDomains = [
    'https://editor.subhub.com',
    'http://editor.subhub.com',
    'https://dev-fir-course-c17d4.firebaseapp.com',
    'http://dev-fir-course-c17d4.firebaseapp.com', 
    'https://dev-fir-course-c17d4.web.app',
    'http://localhost:4200',
    'https://staging-subhub.firebaseapp.com'
];
if (in_array($origin, $allowedDomains)) {
    header('Access-Control-Allow-Origin: ' . $origin);
}
header("Content-Type: application/json; charset=UTF-8");
// header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
header("Access-Control-Allow-Methods: GET,POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

/**
 * id and 'siteid' site db name
 * uid site admin email
 * gsass generate sass file
 * 
 * paymentplan = all list of paymentplnan in drip
 * article_drip = true / false
 * paymentplanid =  paymentplan id
 */

$allowedParameters = ['id', 'uid', 'gsass', 'homepagetype', 'groups', 'dataid', 'artid', 'catid', 'siteid', 'paymentplan', 'article_drip', 'paymentplanid', 'zap', 'zapevents', 'landing-page-events'];
$allowedParametersExist = [];
foreach($_GET as $get => $val) {
    if (in_array($get, $allowedParameters)) {
        $allowedParametersExist[] = $get;
    }
}

/** firebasedataytpe valid value 
 * Live Editor: api.services.ts
 * Local usage eg: http://api_echo.local/index.php?id=sh_jerichotest&uid=abaisa.it@gmail.com&firebasedatatype=general_settings
*/

$allowedFirebaseDataType = ['general_settings', 'sections', 'top_navigation', 'buttom_navigation', 'sidebar'];
$allowedFirebaseDataTypeExist = [];

if (count($allowedParametersExist) > 1) {

    if (!filter_var($_GET['uid'] , FILTER_VALIDATE_EMAIL)) {
        die();
    } 

    if (isset($_GET['firebasedatatype'])) {
        foreach($_GET as $get => $val) { 
            if (in_array($val, $allowedFirebaseDataType)) {
                $allowedFirebaseDataTypeExist[] = $get;
            }
        }

        if (count($allowedFirebaseDataTypeExist) <= 0) {
            die();
        }
    }

    //Live Editor usage: http://site.local/firebase-evolistener?id=sh_jerichotest&uid=developer@subhub.com
    $_GET['uid'] = filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING);
    if (isset($_GET['id']) && isset($_GET['uid'])) {
        $_GET['id'] = isset($_GET['id']) ? filter_var(trim($_GET['id']), FILTER_SANITIZE_STRING) : die();
        if (strpos($_GET['id'], 'sh_') !== false) {
            // $sitename = trim(str_replace('sh_', '', $sitename));
            require_once('post/mailchimp.php');
        }
        die();
    }
    
    //Live Editor usage: http://site.local/firebase-evolistener?gsass=sh_jerichotest&uid=developer@subhub.com&homepagetype=public-homepages (members-homepages)
    if (isset($_GET['gsass'])  && isset($_GET['uid']) && isset($_GET['homepagetype'])) {
        $_GET['gsass'] = isset($_GET['gsass']) ? filter_var(trim($_GET['gsass']), FILTER_SANITIZE_STRING) : die();
        $_GET['homepagetype'] = isset($_GET['homepagetype']) ? filter_var(trim($_GET['homepagetype']), FILTER_SANITIZE_STRING) : die();
        
       
        if (stristr("landing-pages", $_GET['homepagetype'])) { // == 'landing-page') {
            require_once('post/generate_sass_landing.php');
        } else {
            require_once('post/generate_sass.php');
        }
        die();
    }

    //Live Editor usage: http://site.local/firebase-evolistener?groups=sh_jerichotest&uid=developer@subhub.com
    if (isset($_GET['groups']) && isset($_GET['uid'])) {
        $_GET['groups'] = isset($_GET['groups']) ? filter_var(trim($_GET['groups']), FILTER_SANITIZE_STRING) : die();
        require_once('post/neuron_groups.php');
        die();
    }

    //Live Editor usage: http://site.local/firebase-evolistener?dataid=sh_jerichotest&uid=abaisa.it@gmail.com&firebasedatatype=general_settings
    if (isset($_GET['dataid'])  && isset($_GET['uid']) && isset($_GET['firebasedatatype'])) {
        $_GET['dataid'] = isset($_GET['dataid']) ? filter_var(trim($_GET['dataid']), FILTER_SANITIZE_STRING) : die();
        $_GET['firebasedatatype'] = isset($_GET['firebasedatatype']) ? filter_var(trim($_GET['firebasedatatype']), FILTER_SANITIZE_STRING) : die();
        require_once('post/save_firebase_data.php');
        die();
    }

    // Categories
     //Live Editor usage: http://site.local/firebase-evolistener?catid=sh_jerichotest&uid=abaisa.it@gmail.com
    if (isset($_GET['catid'])  && isset($_GET['uid']) ) {
        $_GET['catid'] = isset($_GET['catid']) ? filter_var(trim($_GET['catid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        require_once('post/categories.php');
        die();
    }


    // Articles 
    //Live Editor usage: http://site.local/firebase-evolistener?artid=sh_jerichotest&uid=abaisa.it@gmail.com
    if (isset($_GET['artid'])  && isset($_GET['uid']) ) {
        $_GET['artid'] = isset($_GET['artid']) ? filter_var(trim($_GET['artid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        require_once('post/articles.php');
        die();
    }

    // Drip Content Manager
    // List of paymentplan id 
    // @paymentplan = all
    if (isset($_GET['siteid'])  && isset($_GET['uid']) && isset($_GET['paymentplan'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        $_GET['paymentplan'] = isset($_GET['paymentplan'])? filter_var(trim($_GET['paymentplan']), FILTER_SANITIZE_STRING) : die(); 
        require_once('post/drip_content.php');
        die();
    }

    // Get articles by Id
    // @article = true
    // @paymentplanid int Paymentpla id
    if (isset($_GET['siteid'])  && isset($_GET['uid']) && isset($_GET['article_drip']) && isset($_GET['paymentplanid'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        require_once('post/drip_content.php');
        die();
    }

    // Post Drip Content
    if (isset($_GET['siteid'])  && isset($_GET['uid']) && isset($_GET['published_drip'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();     
        $_GET['published_drip'] = isset($_GET['published_drip']) ? filter_var(trim($_GET['published_drip']), FILTER_SANITIZE_STRING) : die();
        require_once('post/drip_content.php');
        die();
    }

    // Delete Drip Course Content
    if (isset($_GET['siteid'])  && isset($_GET['uid']) && isset($_GET['delete_course_content']) && isset($_GET['paymentplanid'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        $_GET['paymentplan'] = isset($_GET['paymentplan'])? filter_var(trim($_GET['paymentplan']), FILTER_SANITIZE_STRING) : die(); 
        $_GET['delete_course_content'] = isset($_GET['delete_course_content']) ? filter_var(trim($_GET['delete_course_content']), FILTER_SANITIZE_STRING) : die();
        $_GET['course_content_id'] = isset($_GET['course_content_id']) ? filter_var(trim($_GET['course_content_id']), FILTER_SANITIZE_STRING) : die();
        require_once('post/drip_content.php');
        die();
        
    }

    // Get Neuron Events
    // Live Editor usage: http://site.local/firebase-evolistener?siteid=sh_jerichotest&uid=abaisa.it@gmail.com&eventid=events
    if (isset($_GET['siteid']) && isset($_GET['uid']) && isset($_GET['eventid'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        $_GET['eventid'] = isset($_GET['eventid']) ? filter_var(trim($_GET['eventid']), FILTER_SANITIZE_STRING) : die();
        require_once('post/neuron_events.php');
        die();
    }

    // PROCESS MEMBER COURSE FROM ZAP
    // Live Editor usage: https://api.subhub.net/firebase-evolistener/index.php?siteid=sh_eraserheads&uid=eheads@gmail.com&zap=true&zapevents=zap-add-member-courses&paymentplan=32&member_id=62
    if (isset($_GET['siteid']) && isset($_GET['uid']) && isset($_GET['zap']) && isset($_GET['zapevents'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        $_GET['zap'] = isset($_GET['zap']) ? filter_var(trim($_GET['zap']), FILTER_SANITIZE_STRING) : die();
        $_GET['zapevents'] = isset($_GET['zapevents']) ? filter_var(trim($_GET['zapevents']), FILTER_SANITIZE_STRING) : die();
        $_GET['paymentplanid'] = isset($_GET['paymentplanid']) ? filter_var(trim($_GET['paymentplanid']), FILTER_SANITIZE_NUMBER_INT) : die();
        $_GET['member_id'] = isset($_GET['member_id']) ? filter_var(trim($_GET['member_id']), FILTER_SANITIZE_NUMBER_INT) : die();
        require_once('post/evo_zappier.php');
        die();
    }

     // SAVE LANDING PAGE FROM FB TO EVO
    // Live Editor usage: https://api.subhub.net/firebase-evolistener/index.php?siteid=sh_eraserheads&uid=eheads@gmail.com&landing-page-events=landing-page-save
    if (isset($_GET['siteid']) && isset($_GET['uid']) && isset($_GET['landing-page-events'])) {
        $_GET['siteid'] = isset($_GET['siteid']) ? filter_var(trim($_GET['siteid']), FILTER_SANITIZE_STRING) : die();
        $_GET['uid'] = isset($_GET['uid']) ? filter_var(trim($_GET['uid']), FILTER_SANITIZE_STRING) : die();
        $_GET['landingpage-events'] = isset($_GET['landing-page-events']) && !empty($_GET['landing-page-events']) ? filter_var(trim($_GET['landing-page-events']), FILTER_SANITIZE_STRING) : die();
        require_once('post/firebase_to_evo_events.php');
        die();
    }

}

die();

?>