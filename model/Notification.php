<?php

class SlackNotification {

    public function send($message) {
        $data = "payload=" . json_encode(array(
                "username" => "SubHub Logs",
                "text" => $message,
                "icon_url" => 'http://www.subhub.com/subhub_logo_90x90.png',
        ));

        // $ch = curl_init("https://memberdigital.slack.com/services/hooks/incoming-webhook?token=zX5DlMpPUQZUDKTVI5kNhvAB");
        $ch = curl_init("https://hooks.slack.com/services/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");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if (trim($result) != 'ok') {
            mail('joan@subhub.com,developer@subhub.com', '4.0 slack resut', print_r($result, true) . ' message ' . $message);
        }
        return $result;
    }

    public function sendErrorLogs($generatelogs, $errorResponseName, $secure_url) {
        $err = '';
        $count = 0;
        foreach($generatelogs as $key=>$val) {
            if ($count > 0) {
                $err .= "\n" . $val;
            } else {
                $err =  $val;
            }
            $count++;
        }
        $slackMsg = sprintf("<{$secure_url}>\n%s \n%s ",
        "Error Type: {$errorResponseName}", "Logs: {$err}");
        $this->send($slackMsg);
    }

}

?>