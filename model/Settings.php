<?php 
class EvoApiSettings {

    private $dbconnect;
    public $settings;

    public function __construct($db) {
        $this->dbconnect = $db;
        $this->getSettings();
    }

	function getSettings() {

        $settings = $this->dbconnect->GetAll("SELECT * FROM neuron_settings");
		foreach ($settings as $setting) {
			$this->$setting['name']=$setting['value'];
		}
        return $this->settings;
	}

    function tz_load($current,$format = "d/m/Y H:i:s") {
        global $settings;
        
        $sc = $settings->site_timezone * 60 * 60;
    
        if($sc < 0) {
            $str = $sc." seconds";
        } else {
               $str = "+".$sc." seconds";
        }
        
        $t = new DateTime(date('Y-m-d H:i:s',$current));
        
        $t->modify($str);
            
        $tf = $t->format($format);
        
        return $tf;   
    }
    
    /**
     * takes a time-zone adjusted time and returns GMT time for saving.
     * @return unknown_type
     */
    function tz_save($current,$format = "d/m/Y H:i:s") {
        global $settings;
        
        $sc = $settings->site_timezone * 60 * 60;
        
        if($sc < 0) {
            $sc = abs($sc);
            $str = "+".$sc." seconds";
        } else {
            $sc = 0 - $sc;
            $str = $sc." seconds";
        }
    
        $t = new DateTime(date('Y-m-d H:i:s',$current));
        
        $t->modify($str);
            
        $tf = $t->format($format);
        
        return $tf;   
    }
    
    
    function dateTimezoneSet() {
        $timestamp = time();
        $timestamp = strtotime($this->tz_load($timestamp, 'Y-m-d H:i:s'));
        $date_time_array = getdate($timestamp);
        $hours = $date_time_array['hours'];
        $minutes = $date_time_array['minutes'];
        $seconds = $date_time_array['seconds'];
        $month = $date_time_array['mon'];
        $day = $date_time_array['mday'];
        $year = $date_time_array['year'];
    
        $timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
        $date = strftime('%Y-%m-%d', $timestamp);
        return trim($date);    
    }
    
}

?>