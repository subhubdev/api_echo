<?php
require_once(NEURON_ROOT . 'includes/mailchimp_v3/vendor/autoload.php');
use \DrewM\MailChimp\MailChimp;
class PostMailchimp {
    private $dbconnect;
    private $mailchimp_id = '';
    public $logs= array();
    
    public function __construct($db) {
        $this->dbconnect = $db;
    }

    public function getList() {

        $sql = "SELECT `name`, trim(`value`) as apikey FROM neuron_settings WHERE `name` = 'mailchimp_apikey'";
        $result = $this->dbconnect->getRow($sql);
        $this->dbconnect->close();

        if ($result && !empty($result)) {
             $this->mailchimp_id = $result['apikey'];

            if (!empty($this->mailchimp_id)) {
                return $this->getMailChimpList();
            }           
        } 
        $this->logs[] = __CLASS__. "\nSQL: {$sql} \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        return false;
    }

    private function getMailChimpList_ORIG() {

        include_once NEURON_ROOT. 'includes/mailchimp/src/Mailchimp.php';
        try {
            $MailChimp = new MailChimp($this->mailchimp_id);
            $post_arr = array();
            $result = $MailChimp->call('lists/list');
            foreach($result['data'] as $list) {
                $post_item = array(
                    'id' => $list['id'],
                    'name' => $list['name']
                );

                array_push($post_arr, $post_item);
            }
            return $post_arr;
        } 
        catch (Exception $ex) {
            $this->logs[] = __CLASS__. "\n ERROR Unable to get MailChimpList \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . " \n Mailchimp Id: " . $this->mailchimp_id. "\n Exception -" . $ex->getMessage();
            return false;
        }
	}

    // MailChimp Integration V3
    private function getMailChimpList() {
        global $settings;
        $post_arr = array();

        try {
            $MailChimp = new MailChimp($settings->mailchimp_apikey);
            $result = $MailChimp->get('lists');
            foreach($result['lists'] as $list) {
                $post_item = array(
                    'id' => $list['id'],
                    'name' => $list['name']
                );
                array_push($post_arr, $post_item);
            }
            return $post_arr;
        } 
        catch (Exception $ex) {
            $this->logs[] = __CLASS__. "\n ERROR Unable to get MailChimpList \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . " \n Mailchimp Id: " . $this->mailchimp_id. "\n Exception -" . $ex->getMessage();
            return false;
        } 
    }

}

?>