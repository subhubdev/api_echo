<?php
// error_reporting(E_ALL);
require_once(API_ROOT. 'model/FirebaseFunctions.php');
require_once(API_ROOT. 'helper/Variables.php');
require_once(API_ROOT. 'helper/BannerSlider.php');
require_once(API_ROOT. 'helper/Minifier.php');
require_once(API_ROOT. 'helper/SideBar.php');
require_once(API_ROOT. 'model/SaveFirebaseData.php');
class GenerateSassLanding extends FirebaseFunctions {
    private $dbconnect;
    public $logs = array();
    public $homepage = 'public';
    public $siteroot;
    public $trialCreatedDate;
    public $moveToSitesDate = '2022-02-14'; // all trials move to /site folders

    public function __construct($db) {
        global $settings;
        // $this->dbconnect = $db;
        $trialTimestamp = strtotime($settings->trialcreated);
        $this->trialCreatedDate = date("Y-m-d", $trialTimestamp);
        $this->siteroot = strtolower(trim($settings->sitename));
        parent::__construct();
    }

    private function getFirebaseData() {
        $results = array();

        $this->checkPublished($this->homepage);

        // Genetal Settings
        $results['generalsetting'] = $this->getGeneralSettings();

        // Public and Members Homepage
        $landingPages = $this->getLandingPage('landing_page');
        foreach( $landingPages as $key => $landingPageSection ) {
            if ($key == 0) {
                $results['homepagesections'] = $landingPageSection['section'];
            } else {
                $results['homepagesections'] = array_merge($results['homepagesections'], $landingPageSection['section']);
            }
        }
    
        return $results = !empty($results) ? $results : false;
    }

    public function generateSassLanding() {

        global $settings;
        if (strpos($_SERVER['SERVER_NAME'], '.local') !== FALSE) {
            $siteRootFolder = "usr/local/var/www/subhub/sites";
        } else {
            $siteRootFolder = "var/www/html/sites";
        }

       
        $siteName = $this->siteroot;
        $templateName = $settings->template;
        $variables = new Variables();
        $variablesImportsDeclaration = "@import 'generalsettings';" . PHP_EOL; // Add _generalsettings.scss as a start then loop through the other existing sections
        $data_for_compile = $this->getFirebaseData();        
        
        $this->writeGeneralSettignsSASSFile($data_for_compile['generalsetting'][0], $variables);

        //Clear Sections Variable Locally
        $this->clearLocalTemplateSectionsFolder('sections');
        
        foreach ( $data_for_compile['homepagesections']  as $key => $value  ) {
            $layoutVariablesDeclaration = '';
            $layoutSASSFIle = '';
            // To ADD Remaining
            switch (strtolower($value['_menu'])){
                case 'top_navigation':
                    $this->writeHomepageSectionsSASSFile($value, $variables->topNavigationList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables

                break;

                case 'text_section':
                    $this->writeHomepageSectionsSASSFile($value, $variables->textSectionList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables

                break;

                case 'spacer':
                    $this->writeHomepageSectionsSASSFile($value, $variables->spacerList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables

                break;

                case 'showcase':
                    $this->writeHomepageSectionsSASSFile($value, $variables->showcaseList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables

                break;

                case 'footer_block':
                    $this->writeHomepageSectionsSASSFile($value, $variables->footerList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables

                break;

                case 'banner':
                    $this->writeHomepageSectionsSASSFile($value, bannerFields());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'carousel' :
                    $this->writeHomepageSectionsSASSFile($value, sliderFields());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'code_block' :
                    $this->writeHomepageSectionsSASSFile($value, $variables->codeBlockList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'call_to_action':
                    $this->writeHomepageSectionsSASSFile($value,$variables->callToActionList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'article':
                    $this->writeHomepageSectionsSASSFile($value,$variables->articleList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'featured_courses':
                    $this->writeHomepageSectionsSASSFile($value,$variables->featuredCoursesList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'articles_category':
                    $this->writeHomepageSectionsSASSFile($value,$variables->articlesCategoryList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;
                
                case 'text_image':
                    $this->writeHomepageSectionsSASSFile($value,$variables->textImageList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'text_image_full':
                    $this->writeHomepageSectionsSASSFile($value,$variables->textImageFullList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'events':
                    $this->writeHomepageSectionsSASSFile($value,$variables->eventsList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'faq':
                    $this->writeHomepageSectionsSASSFile($value,$variables->faqList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;
                
                case 'text_video':
                    $this->writeHomepageSectionsSASSFile($value,$variables->textVideoFullList());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'testimonial':
                    $this->writeHomepageSectionsSASSFile($value,$variables->testimonialSliderFields());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;

                case 'newsletter':
                    $this->writeHomepageSectionsSASSFile($value,$variables->newsletterFields());
                    $variablesImportsDeclaration = $variablesImportsDeclaration . "@import 'sections/" . $value['_id'] . "_".strtolower($value['_menu'])."';" . PHP_EOL; // Section Variables
                break;
            }
            
        }
        
        $this->writeTemplateVariablesImportsDeclarationFile( $variablesImportsDeclaration );

        // Check if Local for an extra additional to a command
        $extraConfigCommand = '';
        if (strpos($_SERVER['SERVER_NAME'], '.local') !== FALSE) {
            $extraConfigCommand = '/usr/local/bin/node ';
        }

        if (($settings->trialstatus=='TRIAL' &&  $this->trialCreatedDate < $this->moveToSitesDate) || $settings->trialstatus=='EXPIRED') {
            $siteRootFolder = 'trials';
        } 
        
        if (strpos($_SERVER['SERVER_NAME'], '.local') !== FALSE) {
            $siteRootFolder = 'sites';
        }

        $sourceTemplate = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/sass/template.scss';
        if(!is_file($sourceTemplate)){
            $contents = "@import 'variables';";
            file_put_contents($sourceTemplate, $contents);
        }

        $destinationTemplate = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/template.css';
        $minifiedPath = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/template.min.css';


        if(!is_file($destinationTemplate)){
            $contents = "";
            file_put_contents($destinationTemplate , $contents);
        }

        $testDestinationTemplate = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/test_template.css';
        if(!is_file($testDestinationTemplate)){
            $contents = "";
            file_put_contents($testDestinationTemplate, $contents);
        }

        // @execute a TEST on Compile of SASS
		$compileCommad = $extraConfigCommand . '/usr/local/bin/sass ' . $sourceTemplate . ' ' . $testDestinationTemplate;
		@chmod($testDestinationTemplate, 0777);
        @exec($compileCommad . ' 2>&1', $response);
		
		if (count($response) == 0 || strpos(strtolower(implode('',$response)), 'deprecated') !== false || strpos(strtolower(implode('',$response)), 'deprecation') !== false || strpos(strtolower(implode('',$response)), 'env') !== false) {
			$compileCommad = $extraConfigCommand . '/usr/local/bin/sass ' . $sourceTemplate . ' ' . $destinationTemplate;
			@chmod($destinationTemplate, 0777);
            @exec($compileCommad . ' 2>&1', $response);
            
            //Minified CSS
            @chmod($minifiedPath, 0777);
            $minified = new JsCssMinifier();
            $minified->minifyCss($destinationTemplate, $minifiedPath );

            // $minified->minifyJs($r = 's',$t ='d');

		} else {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ."\nError Code: 3". "\n ERROR " . print_r($response, true);
            return 3;
        }

        return 1;

    }

    public function writeGeneralSettignsSASSFile( $generalSettings, $variables ) {
        
        // Get all variables
        // Loop through the data
        // Add values from general settings
        // Prepare string for writing
        global $settings;
        $variableString = "";
        $siteRootFolder = "sites";
        $siteName = $this->siteroot;
        $templateName = $settings->template;

        if ( $generalSettings['_selected_font'] == 'google_fonts' ) {
            foreach ( $variables->googleFontList() as $key => $value ) {
                switch ($key) {
                    case 'url':
                    case 'font_family':
                    case 'generic_font_family':
                        $variableString = $variableString . "$" . $key . ": '" . $generalSettings['google_font'][$key] . "';" . PHP_EOL;
                    break;
                    
                    default:
                        $variableString = $variableString . "$" . $key . ": " . $generalSettings['google_font'][$key] . ";" . PHP_EOL;
                    break;
                }
            }

        } else {
            foreach ( $variables->webFontList() as $key => $value ) {
                $variableString = $variableString . "$" . $key . ": " . $generalSettings['web_font'][$key] . ";" . PHP_EOL;
            }

        }

        foreach ( $variables->spacingList() as $key => $value ) {
            if ($key != 'page_width') {
                switch ($generalSettings['spacing'][$key]) {
                    case 1:
                        $generalSettings['spacing'][$key] = '.25rem';
                    break;
                    
                    case 2:
                        $generalSettings['spacing'][$key] = '.50rem';
                    break;
                    
                    case 3:
                        $generalSettings['spacing'][$key] = '1rem';
                    break;
                    
                    case 4:
                        $generalSettings['spacing'][$key] = '1.5rem';
                    break;
                    
                    case 5:
                        $generalSettings['spacing'][$key] = '3rem';
                    break;
                    
                    default:
                        $generalSettings['spacing'][$key] = '0';
                    break;
                }
            }
            
            $variableString = $variableString . "$" . $key . ": " . $generalSettings['spacing'][$key] . ";" . PHP_EOL;
        }

        foreach ( $variables->colorList() as $key => $value ) {
            $variableString = $variableString . "$" . $key . ": " . $generalSettings['color'][$key] . ";" . PHP_EOL;
        }

        //Change Template name
        $variableString = $variableString . "@import '" . NEURON_ROOT . "designtemplates/firebase/" . $templateName . "/sass/general-settings';"; // GENRAL SETTINGS CSS FROM CORE

        // $variableString = $variableString . "@import '" . NEURON_ROOT . "designtemplates/firebase/Business/sass/general-settings';"; 

        //Add Writing for Custom CSS
        $variableString = $variableString . PHP_EOL . $generalSettings['custom_css'] . PHP_EOL . PHP_EOL . PHP_EOL;

        //Set directory for Writing
        if (($settings->trialstatus=='TRIAL' &&  $this->trialCreatedDate < $this->moveToSitesDate) || $settings->trialstatus=='EXPIRED') {
            $siteRootFolder = 'trials';
        }

        $fileDirectory = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/sass/_generalsettings.scss';

        //Write file
        $result = $this->writeOnFile($fileDirectory, $variableString);

        return $result;
    }

    public function writeHomepageSectionsSASSFile( $homepageSectionData, $homepageSectionVariables ) {
        // Loop Through All the Homepage Section Data
        // Get all variables
        // Loop through the data
        // Add values from top navigation section data
        // Prepare string for writing
        global $settings;
        $variableString = "";
        $siteRootFolder = "sites";
        $siteName = $this->siteroot;
        $templateName = $settings->template;

        foreach ( $homepageSectionVariables as $key => $value ) {
            
            if (is_array($value)) {
                
                foreach ( $value as $key_level_1 => $value_level_1 ) {
                    if (is_array($value_level_1)) {
                        // Check if which layout AND loop key
                        // Yes then proceed looping through the custom fields for that layout
                        foreach ( $value_level_1 as $key_level_2 => $value_level_2 ) {
                            
                            if(is_array($value_level_2 )) {
                                foreach ($value_level_2 as $key_level_3 => $value_level_3) {
                                    if ( $homepageSectionData[$key][$key_level_1][$key_level_2][$key_level_3] ) {
                                        $variableString = $variableString . "$" . $key . "_" . $key_level_1 . "_" . $key_level_2 . "_" . $key_level_3. ": " . $homepageSectionData[$key][$key_level_1][$key_level_2][$key_level_3] . ";" . PHP_EOL;
                                    } else {
                                        $variableString = $variableString . "$" . $key . "_" . $key_level_1 . "_" . $key_level_2 . "_" . $key_level_3 . ": unset;" . PHP_EOL;
                                    }
                                }
                            } 
                            else {
                                // Third Layer of Variables
                                if ( $homepageSectionData[$key][$key_level_1][$key_level_2] ) {
                                    $variableString = $variableString . "$" . $key . "_" . $key_level_1 . "_" . $key_level_2 . ": " . $homepageSectionData[$key][$key_level_1][$key_level_2] . ";" . PHP_EOL;
                                } else {
                                    $variableString = $variableString . "$" . $key . "_" . $key_level_1 . "_" . $key_level_2 . ": unset;" . PHP_EOL;
                                }
                            }
                        }
                    } else {
                        // Second Layer of Variables
                        if ( $homepageSectionData[$key][$key_level_1] ) {
                            $variableString = $variableString . "$" . $key . "_". $key_level_1 .": " . $homepageSectionData[$key][$key_level_1] . ";" . PHP_EOL;
                        } else {
                            $variableString = $variableString . "$" . $key . "_". $key_level_1 .": unset;" . PHP_EOL;
                        }
                    }
                }
                
            } else {
                // First Layer of Variables
                // Fields default for all Layouts
                if ( $homepageSectionData[$key] ) {
                    $variableString = $variableString . "$" . $key . ": " . $homepageSectionData[$key] . ";" . PHP_EOL;
                } else {
                    $variableString = $variableString . "$" . $key . ": unset;" . PHP_EOL;
                }
            }

        }
        //Change Template name
        // LAYOUT SPACIFIC FROM CORE
        $variableString = $variableString . PHP_EOL . PHP_EOL . "#section_id_" . $homepageSectionData['_id'] . "{ " . PHP_EOL . PHP_EOL; 
        $variableString = $variableString . "@import '" . NEURON_ROOT . "designtemplates/firebase/" . $templateName . "/sass/sections/" . strtolower($homepageSectionData['_menu']) . "/layouts/relative/" . strtolower($homepageSectionData['_layout']) . "';";
        // $variableString = $variableString . "@import '" . NEURON_ROOT . "designtemplates/firebase/Business/sass/sections/" . strtolower($homepageSectionData['_menu']) . "/layouts/relative/" . strtolower($homepageSectionData['_layout']) . "';";

        $variableString = $variableString . PHP_EOL . PHP_EOL . "}"; 

        //Set directory for Writing
        if (($settings->trialstatus=='TRIAL' &&  $this->trialCreatedDate < $this->moveToSitesDate) || $settings->trialstatus=='EXPIRED') {
            $siteRootFolder = 'trials';
        }
        // Use menu name as filename for the specific homepage section variable
        $fileDirectory = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/sass/sections/_' . $homepageSectionData['_id'] . '_' . strtolower($homepageSectionData['_menu']) .'.scss';

        //Write file
        $result = $this->writeOnFile($fileDirectory, $variableString);

        return $result;
    }

    public function writeTemplateVariablesImportsDeclarationFile( $variablesImportsDeclarationString ) {
        
        global $settings;
        $siteRootFolder = "sites";
        $siteName = $this->siteroot;
        $templateName = $settings->template;

        //Set directory for Writing
        if (($settings->trialstatus=='TRIAL' &&  $this->trialCreatedDate < $this->moveToSitesDate) || $settings->trialstatus=='EXPIRED') {
            $siteRootFolder = 'trials';
        }
        // Use menu name as filename for the specific homepage section variable
        $fileDirectory = "/$siteRootFolder/" . $siteName . '/design/templates/landing/' . $templateName . '/sass/_variables.scss';

        //Write file
        $result = $this->writeOnFile($fileDirectory, $variablesImportsDeclarationString);

        return $result;
    }

    function writeOnFile($fileDirectory, $stringToWrite) {

        // Make sure to set as 777
        // @chmod($fileDirectory, 0777);
        exec ("find $fileDirectory -type d -exec chmod 0777 {} +");
        exec ("find $fileDirectory -type f -exec chmod 0777 {} +");
        // Open for writing only; place the file pointer at the beginning of the file 
        // and truncate the file to zero length. If the file does not exist, attempt to create it.
        $sassFile = fopen($fileDirectory, "w");

		if (is_writable($fileDirectory)) {

            // $var = fread($sassFile, filesize($fileDirectory));

			$result = fwrite($sassFile, $stringToWrite);
		
			if ( $result === FALSE) {
                $this->logs[] = __CLASS__.'\nUnable to write: {$sassFile} \n Function name : ' . __FUNCTION__ . ' Line number ' . __LINE__ . '\n ERROR ' .$sassFile;
				echo "Cannot write to file ($sassFile)";
				exit;
            }
            
            fclose($sassFile);
            return 1;
        } else {
            $this->logs[] = __CLASS__.'\nUnwritable File Directory \n Function name : ' . __FUNCTION__ . ' Line number ' . __LINE__ . '\n ERROR ' .$fileDirectory;
        }
        
        return false;
    }

    public function clearLocalTemplateSectionsFolder($directory) {
        
        global $settings;
        
        $siteRootFolder = "sites";
        $siteName = $this->siteroot;
        $templateName = $settings->template;

        //Set directory for Writing
        if (($settings->trialstatus=='TRIAL' &&  $this->trialCreatedDate < $this->moveToSitesDate) || $settings->trialstatus=='EXPIRED') {
            $siteRootFolder = 'trials';
        }

        $landingTemplateDirectory = "/$siteRootFolder/" . $siteName . '/design/templates/landing';
        $fileDirectory = '/' . $landingTemplateDirectory . $templateName . '/sass/' . $directory;

        $this->createLandingTemplateFileDirectory($landingTemplateDirectory, $templateName);
        $this->checkDirectory($fileDirectory);
        //Clear Folder
        foreach(scandir($fileDirectory) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$fileDirectory/$file")) rmdir_recursive("$fileDirectory/$file");
            else unlink("$fileDirectory/$file");
        }
        return $result;
    }

    private function checkDirectory($directory) {
        if (!file_exists(dirname($directory))) {
            mkdir(dirname($directory, 0777));
        }
    }

    private function createLandingTemplateFileDirectory($directory, $templateName) {
        if (!file_exists($directory)) {
            mkdir($directory, 0777);
        }

        if (!file_exists($directory . '/' . $templateName)) {
            exec('cp -rp ' . dirname($directory) . '/' . $templateName . ' ' . $directory);
        }
    }

}
