<?php

require_once(API_ROOT. 'cloud-firestore/vendor/autoload.php');

use Google\Cloud\Firestore\FirestoreClient;
// error_reporting(E_ALL);
class FirebaseFunctions {

    private $firestore;
    private $site_collection = '';
    public $checkPublished = true;
    private $site_environment; // staging / production 
    public $secure_url ='';
    public $landingPageExt = 'pages';

    function __construct () {
        global $settings;
        $this->site_collection = $settings->firebase_collection;
        $this->site_environment = $settings->site_environment;
        $this->secure_url = $settings->secure_url;
        $this->landingPageExt = isset($settings->landing_page_extension) ? $settings->landing_page_extension : $this->landingPageExt;
        $config = $this->config();
        $this->firestore = new FirestoreClient($config);
    }

    private function config() {

        //Production
        // $path = '/opt/firebase/firebase-credentials-production.json';
        // return  array(
        //     'projectId' => 'subhub-cd447',
        //     "keyFile" => json_decode(file_get_contents($path), true)
        // );

        $serviceAccount = (substr($_SERVER['SERVER_NAME'], -5) == 'local') || $this->site_environment == 'staging' ? '/opt/firebase/firebase-credentials-staging.json' : '/opt/firebase/firebase-credentials-production.json';
        $projectId  = (substr($_SERVER['SERVER_NAME'], -5) == 'local') || $this->site_environment == 'staging' ? 'fir-course-c17d4' : 'subhub-cd447';
        return  array(
            'projectId' => $projectId,
            "keyFile" => json_decode(file_get_contents($serviceAccount), true),
        );
    }

    public function getGeneralSettings() {
        $result = Array();
        $docRef = $this->firestore->collection("{$this->site_collection}")->document('general_settings');
        $snapshot = $docRef->snapshot();
        $result[] = $snapshot->exists() ? $snapshot->data() : $result;
        return $result;
    }

    public function getTopNavigationLinks($homepageType = 'public') {
        $result = Array();

        if ( $homepageType == 'public' ) {
            $query = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/public_homepage/navigation/published/top")->orderBy('position', 'ASC') : $this->firestore->collection("{$this->site_collection}/public_homepage/navigation/unpublished/top")->orderBy('position', 'ASC');
        } else {
            $query = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/members_homepage/navigation/published/top")->orderBy('position', 'ASC') : $this->firestore->collection("{$this->site_collection}/members_homepage/navigation/unpublished/top")->orderBy('position', 'ASC');
        }
        
        $querySnapshot = $query->documents();
        foreach ($querySnapshot as $document) {
            $value = $document->data();

            if (isset($value['_sub_menus']) && $value['_sub_menus']) {
                $temp_sub_menu = array();
                foreach ($value['_sub_menus'] as $key => $sub_menu) {
                    $temp_sub_menu[$key] = $value[$key]['position'];
                }
                asort($temp_sub_menu);
                $value['_sub_menus'] = $temp_sub_menu;
                
                $result[] = $value;
            } else {
                $result[] = $value;
            }
        }
        return $result;
    }

    public function getBottomNavigationLinks($homepageType = 'public') {
        $result = Array();
        
        if ( $homepageType == 'public' ) {
            $query = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/public_homepage/navigation/published/bottom")->orderBy('position', 'ASC') : $this->firestore->collection("{$this->site_collection}/public_homepage/navigation/unpublished/bottom")->orderBy('position', 'ASC');    
        } else {
            $query = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/members_homepage/navigation/published/bottom")->orderBy('position', 'ASC') : $this->firestore->collection("{$this->site_collection}/members_homepage/navigation/unpublished/bottom")->orderBy('position', 'ASC');
        }
        
        $querySnapshot = $query->documents();
        
        foreach ($querySnapshot as $document) {
            $result[] = $document->data();
        }
        return $result;
    }

    public function getSidebarBlock($homepageType = 'public') {
        $result = Array();
        
        if ( $homepageType == 'public' ) {
            $sidebarRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/public_homepage/published_sidebar") : $this->firestore->collection("{$this->site_collection}/public_homepage/unpublished_sidebar");    
        } else {
            $sidebarRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/members_homepage/published_sidebar") : $this->firestore->collection("{$this->site_collection}/members_homepage/unpublished_sidebar");
        }
        
        $query =  $sidebarRef->where('_hide', '=', false)->orderBy('_position', 'ASC');
        $documents = $query->documents();
        foreach ($documents as $document) {
            if ($document->exists()) {
                $document_data = $document->data();
                
                if ( $document_data['_menu'] === 'menu_block' ) {
                    $menublock = [];
                    foreach ($document_data as $key => $row) {
                        if( strpos($key,'_nav_menu_') !== false) {
                            $menublock[$key]  = $row;
                        }
                    }
                    if (count($menublock) > 0)  {
                        $pos = array_column($menublock, 'position');
                        array_multisort($pos, SORT_ASC, $menublock);
                        $document_data['sortedmenulinks'] = $menublock;
                        $result[] = $document_data;
                    } 
                } 
                else {
                    $result[] = $document_data;
                } 
            } 
        }
        return $result;
    }

    public function getHomepageSections($homepageType = 'public') {
        $result = Array();

        if ( $homepageType == 'public' ) {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/public_homepage/published") : $this->firestore->collection("{$this->site_collection}/public_homepage/unpublished");    
        } else {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/members_homepage/published") : $this->firestore->collection("{$this->site_collection}/members_homepage/unpublished");    
        }
        
        $query =  $sectionRef->where('_hide', '=', false)->orderBy('_position', 'ASC');
        $querySnapshot = $query->documents();
        foreach ($querySnapshot as $document) {
                $result[] = $document->exists() ? $document->data() : $result;
        }
        return $result;
    }

    public function getCustomizeHomepages() {
        $result = Array();

        $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/customize_homepage/homepages") : $this->firestore->collection("{$this->site_collection}/customize_homepage/homepages");    
 
        $query =  $sectionRef->orderBy('_position', 'ASC');
        $querySnapshot = $query->documents();
        $counter = 0;
        foreach ($querySnapshot as $document) {
                if ($document->exists()) {
                    $result[] = $document->data();
                    $result[$counter]['sections'] = $this->getCustomizeHomepageSections($document->id());
                }
                $counter++;
        }
        return $result;
    }

    public function getCustomizeHomepageTopNavigation() {
        $result = Array();

        $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/customize_homepage/common_homepages") : $this->firestore->collection("{$this->site_collection}/customize_homepage/homepages");    
 
        $query =  $sectionRef->orderBy('_position', 'ASC');
        $querySnapshot = $query->documents();
        $counter = 0;
        foreach ($querySnapshot as $document) {
                if ($document->exists()) {
                    $result[] = $document->data();
                    $this->checkPublishedCustomizedHomepage = $result[$counter]['published'];
                }
                $counter++;
        }
        return $result;
    }

    public function getCustomizeHomepageSections($documentId) {
        $result = Array();

        $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/customize_homepage/homepages/{$documentId}/published") : $this->firestore->collection("{$this->site_collection}/customize_homepage/homepages/{$documentId}/unpublished");
        $query =  $sectionRef->where('_hide', '=', false)->orderBy('_position', 'ASC');
        $querySnapshot = $query->documents();
        foreach ($querySnapshot as $document) {
                $result[] = $document->exists() ? $document->data() : $result;
        }
        return $result;
    }

    public function getTopNavAndFooterSections($homepageType = 'public') {
        $result = array();

        if ( $homepageType == 'public' ) {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/public_homepage/published")->where('_hide', '=', false) : $this->firestore->collection("{$this->site_collection}/public_homepage/unpublished")->where('_hide', '=', false);    
        } else {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/members_homepage/published")->where('_hide', '=', false) : $this->firestore->collection("{$this->site_collection}/members_homepage/unpublished")->where('_hide', '=', false);    
        }
        
        $topNavquery =  $sectionRef->where('_menu', '=', 'top_navigation')->orderBy('_position', 'ASC');         
        $footerQuery = $sectionRef->where('_menu', '=', 'footer_block')->orderBy('_position', 'ASC');

        $topNavSnapshot = $topNavquery->documents();
        foreach ($topNavSnapshot as $document) {
            $result[] = $document->exists() ? $document->data() : $result;
        }

        $footerSnapshot = $footerQuery->documents();
        foreach ($footerSnapshot as $document) {
            $result[] = $document->exists() ? $document->data() : $result;
        }

        return $result;
    }

    public function checkPublished($homepageType = 'public') {

        if ($homepageType == 'landing-page') {
            $docRef = $this->firestore->collection("{$this->site_collection}")->document('landing_page');
        } 
        else {
            $docRef = $homepageType == 'public' ? $this->firestore->collection("{$this->site_collection}")->document('public_homepage') : $this->firestore->collection("{$this->site_collection}")->document('members_homepage');
        }
        
        $snapshot = $docRef->snapshot();
        if($snapshot->exists()) {
            $document_data = $snapshot->data();
            return $this->checkPublished = $document_data['published'];
        }
    }

    public function getDripContenSections($homepageType = 'members', $planId) {
        $result = Array();

        if ($homepageType == 'members') {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/drip_content/published") : $this->firestore->collection("{$this->site_collection}/drip_content/unpublished");    
        }

        $query =  $sectionRef->where('_plan_id', '=', $planId)->orderBy('_position', 'ASC');
        $querySnapshot = $query->documents();
        foreach ($querySnapshot as $document) {
                $result[] = $document->exists() ? $document->data() : $result;
        }
        return $result;
    }

    public function getMyAccountNavigation() {
        global $member;
        $memberId = $member ? $member->id : '';
        return array (
            1 => array (
              'id' => '1',
              'position' => '1',
              'static' => true,
              'text' => 'Change Password',
              '_sub_menu' => false,
              'url' => '/user_preferences?stage=password'
            ),
            2 => array(
              'id' => '2',
              'position' => '2',
              'static' => true,
              'text' => 'Change Personal Details',
              '_sub_menu' => false,
              'url' => '/user_preferences?stage=details'
            ),
            3 => array(
              'id' => '3',
              'position' => '3',
              'static' => true,
              'text' => 'My Profile',
              '_sub_menu' => false,
              'url' => "/profiles/edit/{$memberId}"
            ),
            4 => array(
              'id' => '4',
              'position' => '4',
              'static' => true,
              'text' => 'My Orders',
              '_sub_menu' => false,
              'url' => '/user_preferences?stage=myorders'
            ),
            5 => array(
              'id' => '5',
              'position' => '5',
              'static' => true,
              'text' => 'My Favourites',
              '_sub_menu' => false,
              'url' => '/favorites'
            ),
            6 => array(
              'id' => '6',
              'position' => '6',
              'static' => true,
              'text' => 'My Courses',
              '_sub_menu' => false,
              'url' => '/my-courses'
            ),
            7 => array (
              'id' => '7',
              'position' => '7',
              'static' => true,
              'text' => 'Payment Methods',
              '_sub_menu' => false,
              'url' => '/payment_methods'
            ),
            8 => array(
              'id' => '8',
              'position' => '8',
              'static' => true,
              'text' => 'Manage Subscriptions',
              '_sub_menu' => false,
              'url' => '/manage_subscriptions'
            ),
            9 => array(
              'id' => '9',
              'position' => '9',
              'static' => true,
              'text' => 'Contact Team',
              '_sub_menu' => false,
              'url' => '/contact'
            )
        );
    }

   
    /* L A N D I N G P A G E */

    public function getLandingPage($homepageType = 'landing_page') {
        $result = Array();

        if ( $homepageType == 'landing_page' ) {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/$homepageType/published") : $this->firestore->collection("{$this->site_collection}/$homepageType/unpublished");    
            
            $query =  $sectionRef->where('disabled', '=', false);
            $querySnapshot = $query->documents();
            $counter = 0;
            foreach ($querySnapshot as $document) {
    
                if ($document->exists()) {
                    $result[] = $document->data();
                    $result[$counter]['section'] = $this->getLandingPageSection($document->id(), $homepageType);
                }
                $counter++;
            }
        } 

        return $result;
    }

    public function getMainLandingPage($homepageType = 'landing_page') {
        $result = Array();

        if ( $homepageType == 'landing_page' ) {
            $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/$homepageType/published") : $this->firestore->collection("{$this->site_collection}/$homepageType/unpublished");    
        
            $query =  $sectionRef->where('disabled', '=', false);
            $querySnapshot = $query->documents();
            $counter = 0;
            foreach ($querySnapshot as $document) {
    
                if ($document->exists()) {
                    $result[] = $document->data();
                    $result[$counter]['document_id'] = $document->id();
                    $result[$counter]['url'] = "https://".$this->secure_url .'/'.$this->landingPageExt.'/'. $result[$counter]['slug'];
                }
                $counter++;
            }
        } 
        
        return $result;
    }

    public function getLandingPageSection($documentId, $homepageType = 'landing_page') {
        $result = Array();

        $sectionRef = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/$homepageType/published/$documentId/sections") : $this->firestore->collection("{$this->site_collection}/$homepageType/unpublished/$documentId/sections");
        $query =  $sectionRef->where('_hide', '=', false)->orderBy('_position', 'ASC');
        $querySnapshot = $query->documents();
        foreach ($querySnapshot as $document) {
            $result[] = $document->exists() ? $document->data() : $result;
        }
        return $result;
    }

    public function getLandingTopNavigationLinks($documentId, $homepageType = 'landing-page') {
        $result = Array();

        if ( $homepageType == 'landing-page' ) {
            $query = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/$homepageType/$documentId/top_navigation") : $this->firestore->collection("{$this->site_collection}/$homepageType/$documentId/top_navigation");;
            $querySnapshot = $query->documents();
            foreach ($querySnapshot as $document) {
                $value = $document->data();
    
                if (isset($value['_sub_menus']) && $value['_sub_menus']) {
                    $temp_sub_menu = array();
                    foreach ($value['_sub_menus'] as $key => $sub_menu) {
                        $temp_sub_menu[$key] = $value[$key]['position'];
                    }
                    asort($temp_sub_menu);
                    $value['_sub_menus'] = $temp_sub_menu;
                    
                    $result[] = $value;
                } else {
                    $result[] = $value;
                }
            }
        }
        
        return $result;
    }

    public function getLandingBottomNavigationLinks($documentId, $homepageType = 'landing_page') {
        $result = Array();
        
        if ( $homepageType == 'landing-page' ) {
            $query = $this->checkPublished ? $this->firestore->collection("{$this->site_collection}/$homepageType/$documentId/footer_block") : $this->firestore->collection("{$this->site_collection}/$homepageType/$documentId/footer_block");
            $querySnapshot = $query->documents();
        
            foreach ($querySnapshot as $document) {
                $result[] = $document->data();
            }
        }
        
        return $result;
    }


}