<?php
class UserAccount {

    private $dbconnect;
    public $logs = array();

    public function __construct($db) {
        $this->dbconnect = $db;  
    }

    public function checkAdminEmail($email) {
        $email = strtolower($email);
        $sql = "SELECT admin, id, name, email FROM neuron_members WHERE LOWER(email) > '' AND LOWER(email) = '{$email}' AND admin IN ('1','2') LIMIT 1;";
        $result = $this->dbconnect->GetAll($sql);
        // $this->dbconnect->close();
        
        if ($result) {
            return true;
        }
        $this->logs = __CLASS__."\nSQL: {$sql} \nresult var: $result \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        return false;
    }

}