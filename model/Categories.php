<?php

class ApiCategories {
    private $dbconnect;
    public $logs= array();
    
    public function __construct($db) {
        $this->dbconnect = $db;
    }

    public function getAllCategories() {
        
        $sql = "SELECT `id`,
                        `shortname`,
                        `type`,
                        `parent`,
                        `lft`,
                        `rgt`,
                        `title`,
                        `description`,
                        `body`,
                        `created`,
                        `published`,
                        `modified`,
                        `state`,
                        `cat_parent`,
                        `rel_pos`
                FROM `neuron_categories`;";
        $result = $this->dbconnect->getAll($sql);
        // $this->dbconnect->close();

        if ($result) {
         return $result;
        } 

        $this->logs[] = __CLASS__. "\nSQL: {$sql} \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        return [];
    }

}
?>