<?php

class ApiArticles {
    private $dbconnect;
    public $logs= array();
    
    public function __construct($db) {
        $this->dbconnect = $db;
    }

    public function getAllArticles() {
        
        $sql = "SELECT `id`,
                    `category`,
                    `type`,
                    `shortname`,
                    `title`,
                    `description`,
                    `body`,
                    `created`,
                    `published`,
                    `modified`,
                    `state`,
                    `hideArticleTitle`
                FROM `neuron_articles`;";
        $result = $this->dbconnect->getAll($sql);
        // $this->dbconnect->close();

        if ($result) {
         return $result;
        } 

        $this->logs[] = __CLASS__. "\nSQL: {$sql} \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        return [];
    }

}
?>