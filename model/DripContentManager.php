<?php
require_once('encryption.php');
class DripContentManager {

    private $dbconnect;
    public $logs= array();

    public function __construct($db) {
        $this->dbconnect = $db;
    }

    public function getPaymentPlans() {
        $plans = [];
        $enc = new Encryption();
        $sql = "SELECT DISTINCT
                    content.id, 
                    content.courses_link_shortname as shortname,
                    content.title,
                    content.description,
                    content.frequency,
                    content.position,
                    content.drip_content,
                    content.drip_frequency,
                    content.hasimage,
                    amount.currency,
                    amount.amount
                FROM
                    neuron_paymentplans AS content,
                    neuron_paymentplans_currencies AS amount,
                    neuron_currencies AS currency
                WHERE
                    content.id = amount.paymentplan_id
                AND amount.currency =  (CASE WHEN (SELECT `value` FROM jol_payments_settings WHERE `name` = 'default_currency') IS NULL THEN (SELECT `value` FROM neuron_settings WHERE `name` = 'currencies') ELSE (SELECT `value` FROM jol_payments_settings WHERE `name` = 'default_currency') END )
                AND currency.live > 0 AND content.drip_content = 1 AND content.disable_plan = 0 ORDER BY content.position ASC;";

        $res = $this->dbconnect->getAll($sql); 
        foreach ($res as $k=>$v) {
            // $encid = $enc->sys_encrypt($v['id']);
            $v['enc_plan_id'] = $v['shortname']; //$encid;
            $v['description'] = html_entity_decode($v['description'], ENT_QUOTES, "UTF-8");
            $v['title'] = html_entity_decode($v['title'], ENT_QUOTES, "UTF-8");
            $plans[] = $v;
        }
        return $plans;

    }

    public function getArticlesByPlanId($planId) {
        $plans = [];
        $sql = sprintf("SELECT a.id, a.category, a.shortname, a.title, (SELECT title FROM neuron_categories WHERE id = a.category) as category_title, "
            . "(SELECT dcs.frequency FROM drip_content_schedule dcs WHERE dcs.plan_id = %d AND dcs.article_id = a.id LIMIT 1) as frequency, "
            . "(SELECT dcs.delay FROM drip_content_schedule dcs WHERE dcs.plan_id = %d AND dcs.article_id = a.id LIMIT 1) as delay "
            . "FROM neuron_articles a "
            . "WHERE a.drip_content = 1 AND a.state = 2 AND a.id NOT in (SELECT DISTINCT(article_id) FROM neuron_articles_groups) AND a.title > '' "
            . "ORDER BY delay, a.created DESC", $planId, $planId);

        $res = $this->dbconnect->getAll($sql);
        foreach ($res as $k=>$v) {
            $v['title'] = html_entity_decode($v['title'], ENT_QUOTES, "UTF-8");
            $v['category_title'] = html_entity_decode($v['category_title'], ENT_QUOTES, "UTF-8");
            $plans[] = $v;
        }
        return $plans;
    }

    public function saveDripContent($data) {
        $sqlValues = '';
        $sqlCourseContent = '';
        
        $planid = $data['_id'];
        $frequency = $data['drip_frequency'];
        $planData = [];
        $planData['id'] = $planid; 
        $planData['description'] = htmlspecialchars($data['course_description'], ENT_QUOTES, 'UTF-8');
        $planData['hasImage'] = !empty($data['cover_image']) ? 1 : 0;
        $planData['cover_image'] = $data['cover_image'];
        $planData['drip_frequency'] = $data['drip_frequency'];

        // Update Subscription Plan
        $planid = (int)$planid;
        if ($planid > 0) {
            
            $this->updatePaymentplanCourse($planData);

            // Course Content
            $courseData = $data['course_content'];
            $course_seqno = 1;
            foreach ($courseData as $k=>$value) {
                $title = $value['title'];
                $desc = $value['description'];
                $sqlCourseContent .= sprintf("(%d, %d, '%s', '%s', %d, NOW(), NOW())", $planid, $course_seqno, str_replace("'", "''", $title),  str_replace("'", "''", $desc), $course_seqno) . ",";

                $articles = $value['articles'];
                        $seqno = 1;
                        foreach ($articles as $j=>$artVal) {
                            if(!empty($artVal)) {
                                if (is_numeric($artVal['delay'])) {
                                    $sqlValues .= sprintf("(%d, %d, %d, '%s', %d, %d)",  $planid, $artVal['id'],  $artVal['delay'], $frequency,  $course_seqno ,$seqno) . ",";
                                    $seqno++;
                                }
                            }
                        }

                    $course_seqno++;
                }

            // die();
            if (!empty($sqlValues)) {

                $this->deleteDripScheduleByPlanId($planid);
                $this->saveDripSchedule($sqlValues);

                if (!empty($sqlCourseContent)) {
                    $this->deleteCourseContentByPlanId($planid);
                    $this->updateCourseContent($sqlCourseContent);
                }
            }
            else {
                $this->deleteDripScheduleByPlanId($planid);
                $this->deleteCourseContentByPlanId($planid);
            }
        }
    }

    private function saveDripSchedule ($sqlValues) {
        $sqlValues = substr($sqlValues, 0, -1); 
        $sql = "INSERT INTO `drip_content_schedule` (`plan_id`, `article_id`, `delay`, `frequency`, `drip_course_content_id`, `seqno` ) 
        VALUES ".$sqlValues;
        $res = $this->dbconnect->execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
    }

    private function deleteDripScheduleByPlanId($planId) {
        $sql = sprintf("DELETE FROM drip_content_schedule WHERE plan_id = %d",  $planId);
        $res = $this->dbconnect->execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
    }

    private function deleteCourseContentByPlanId($planId) {
        $sql = sprintf("DELETE FROM drip_course_content WHERE plan_id = %d",  $planId);
        $res = $this->dbconnect->execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
    }

    function deleteCourseContentByContentIdAndPlanId($contentId) {
        $sql = "DELETE FROM drip_course_content WHERE id = {$contentId}";
        $res = $this->dbconnect->Execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
    }

    function unlinkCourseContentInDripSchedule($contentId, $planId) {
        $sql = "UPDATE FROM drip_content_schedule SET drip_course_content_id = null WHERE plan_id = {$planId} AND drip_course_content_id = {$contentId}";
        $res = $this->dbconnect->Execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
    }

    function updatePaymentplanCourse($planData) {
        $sql =  sprintf("UPDATE `neuron_paymentplans` SET `description` = '%s', hasimage = %d, cover_image = '%s', drip_frequency = '%s' WHERE id = %d",
                        $planData['description'], $planData['hasImage'], $planData['cover_image'], $planData['drip_frequency'], $planData['id']);
        $res = $this->dbconnect->Execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
        
    }

    function updateCourseContent($courseData) {
        $courseData = substr($courseData, 0, -1); 
        $sql =  "INSERT INTO `drip_course_content`
                (`plan_id`,
                `content_id`,
                `title`,
                `description`,
                `seqno`,
                `modified_on`,
                `created_on`)
                VALUES ". $courseData;
        $res = $this->dbconnect->Execute($sql);
        if (!$res) {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n SQL: " .$sql;
        }
    }

    // @$data['paymentplanid'] = Plan Id
    // @$member_id
    function addDripContent($data) {
        global $settings;
 
        $paymentplan =  $this->dbconnect->getRow("SELECT id,drip_content FROM neuron_paymentplans WHERE id = {$data['paymentplanid']} ;"); // new PaymentPlan($data['paymentplanid']); 
            
        if ($paymentplan) { 
            $createdDate = $settings->dateTimezoneSet();
            // Check if the plan is under a group where a user should get access automatically to a course, which is being set in member manager manage group
            /* CREATE TABLE `neuron_group_courses` (`group_id` INT NULL,`course_id` INT NULL); */
            $sql = sprintf("SELECT b.group_id, b.course_id, c.disable_plan
            FROM neuron_paymentplans_groups a 
            inner join neuron_group_courses b ON a.group_id = b.group_id 
            inner join neuron_paymentplans c  ON b.course_id = c.id
            where a.paymentplan_id = %d ", $data['paymentplanid']);
            $res1 = $this->dbconnect->getAll($sql);
            
            if ($res1) {
               
                foreach ($res1 as $value) {
                    if ($value['course_id'] != $data['paymentplanid'] && $value['disable_plan'] == '0') { 
                        $sql = sprintf("SELECT * FROM `drip_content_schedule` WHERE plan_id = %d AND delay=0", $value['course_id']);
                        $res = $this->dbconnect->getAll($sql);
                        
                        if ($res) {
                            foreach ($res as $k=>$v) {
                                $sqlExists = sprintf("SELECT id FROM members_drip_content WHERE member_id = %d AND article_id = %d AND plan_id = %d", $data['member_id'], $v['article_id'], $value['course_id']);
                                $exists = $this->dbconnect->getRow($sqlExists);
                                if (empty($exists['id'])) {
                                    $sql2 = sprintf("INSERT INTO members_drip_content (member_id, article_id, plan_id, created)
                                        VALUES (%d, %d, %d, '%s')",  $data['member_id'], $v['article_id'], $value['course_id'], $createdDate);
                                    $this->dbconnect->execute($sql2);
                                }
                            }
                            
                            //insert into member_courses 
                            
                            $sqlCExists = sprintf("SELECT id FROM neuron_members_courses WHERE member_id = %d AND plan_id = %d", $data['member_id'], $value['course_id']);
                            $existsC = $this->dbconnect->getRow($sqlCExists);
                            if (empty($existsC['id'])) {
                                $sql3= sprintf("INSERT INTO `neuron_members_courses` (`member_id`, `plan_id`, `created`) 
                                    VALUES (%d, %d, '%s')",  $data['member_id'], $value['course_id'], $createdDate);
                                $this->dbconnect->execute($sql3);
                            }
                        }
                    }
                    
                }
            }

            if ($paymentplan['drip_content'] == '1')   {

                $sql = sprintf("SELECT * FROM `drip_content_schedule` WHERE plan_id = %d AND delay=0", $data['paymentplanid']);
                $res = $this->dbconnect->getAll($sql);

                if ($res) {
                    
                    $sql ="INSERT INTO  members_drip_content (
                        article_id,
                        plan_id,
                        member_id,
                        created
                        )
                        SELECT * FROM (
                            SELECT DISTINCT
                                s.article_id,
                                s.plan_id,
                                {$data['member_id']} AS member_id,
                                DATE_FORMAT('{$createdDate}', '%Y-%m-%d') AS created
                            FROM
                                drip_content_schedule s
                            WHERE
                                s.plan_id = {$data['paymentplanid']} AND s.delay = 0) AS tmp
                        WHERE NOT EXISTS (
                            SELECT id FROM members_drip_content  WHERE member_id = {$data['member_id']} and  plan_id = {$data['paymentplanid']} 
                        );";
                    $this->dbconnect->execute($sql);

                    //insert into member_courses 
                    $sqlExists = sprintf("SELECT id FROM neuron_members_courses WHERE member_id = %d AND plan_id = %d", $data['member_id'], $data['paymentplanid']);
                    $exists = $this->dbconnect->getRow($sqlExists);
                    if (empty($exists['id'])) {
                        $sql3= sprintf("INSERT INTO `neuron_members_courses` (`member_id`, `plan_id`, `created`) 
                        VALUES (%d, %d, '%s')", $data['member_id'], $data['paymentplanid'], $createdDate);
                        $this->dbconnect->execute($sql3);
                    }

                }
            }
            return true;
        }
        else {
            $this->logs[] = __CLASS__."\n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ . "\n Ivalid Plan Id: " .$data;
            return false;
        }
    }

}

?>