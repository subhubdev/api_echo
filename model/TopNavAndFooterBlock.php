<?php
require_once(API_ROOT. 'model/FirebaseFunctions.php');
require_once(NEURON_ROOT.'includes/neuron/firebase/structure/top-navigation/Top-Navigation.php');
require_once(NEURON_ROOT.'includes/neuron/firebase/structure/footer/Footer.php');

class ApiTopNavAndFooter extends FirebaseFunctions {
    private $footerData;
    private $topNavData;
    private $data;
    public $homepageType = 'public';
    public $viewAsVisitors;

    function __contstruct() {
        parent::__construct();
        // $this->homepageType = $homepageType;
    }

    public function init() {

        $this->data = $this->getTopNavAndFooterSections($this->homepageType);
        $this->topNavData = array_filter($this->data , function($val) { return ($val['_menu'] == 'top_navigation'); });
        $this->footerData = array_filter($this->data , function($val) { return ($val['_menu'] == 'footer_block'); });

        if ($this->topNavData) {
            $this->data['top_nav_data'] = $this->initTopNavigation();
        }

        if ($this->footerData ) {
            $this->data['footer_data']= $this->initFooterBlock();
        }
        $this->data['top_navigation_links'] = $this->getTopNavigationLinks($this->homepageType);
        $this->data['bottom_navigation_links'] = $this->getBottomNavigationLinks($this->homepageType);
         
        $tfData = $this->data;
        foreach( $tfData as $k => $v) {
            if (is_numeric($k)) {
                if (isset($tfData[$k])) {
                    unset($this->data[$k]);
                }
            }

        } 
        return $this->data;
    }

    private function initTopNavigation() {
        $topNav = new TopNavigation();
        $topNavArr = array();
        foreach ($this->topNavData as $topNavKey => $topNavVal ){
            $topNavArr = $topNav->initializeTopNavigationData($this->topNavData[$topNavKey]);
        break;
            //set array for multiple topnav section $topNavArr[] = $topNav->initializeTopNavigationData($this->topNavData[$topNavKey]);
        }
        
        return $topNavArr;
    }

    private function initFooterBlock() {
        $footer = new Footer();
        $footerArr = array();
        foreach ($this->footerData as $footerKey => $footerVal ){
            $footerArr = $footer->initializeFooterData($this->footerData[$footerKey]);
            // sett array for multiple footer block section $footerArr[] = $footer->initializeFooterData($this->footerData[$footerKey]);
        }

        return $footerArr;
    }
}