<?php

class ApiEvents {
    private $dbconnect;
    public $logs= array();
    
    public function __construct($db) {
        $this->dbconnect = $db;
    }

    public function getAllEvents() {

        $sql = "SELECT 
                `nEvents`.`id`,
                `nEvents`.`shortname`,
                `nEvents`.`title`,
                `nEvents`.`description`,
                `nEvents`.`body`,
                `nEvents`.`startdate`,
                `nEvents`.`enddate`,
                `nEvents`.`location`,
                `nEvents`.`created`,
                `nEvents`.`published`,
                `nEvents`.`modified`,
                `nEvents`.`state`,
                `nEvents`.`cover_image`
            FROM
                `neuron_events` AS nEvents
            WHERE `nEvents`.`startdate` >= NOW() ORDER BY startdate ASC LIMIT 12;";
        $result = $this->dbconnect->getAll($sql);

        if ($result) {
            return $result;
        } 

        // $this->logs[] = __CLASS__. "\nSQL: {$sql} \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        // return [];
    }
}

?>