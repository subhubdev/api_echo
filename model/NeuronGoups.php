<?php
class NeuronGroups {
    private $dbconnect;
    public $logs= array();
    
    public function __construct($db) {
        $this->dbconnect = $db;
    }

    public function getGroups() {
        
        $sql = "SELECT * FROM neuron_groups ORDER BY `name`";
        $result = $this->dbconnect->getAll($sql);
        // $this->dbconnect->close();

        if ($result) {
         return $result;
        } 

        $this->logs[] = __CLASS__. "\nSQL: {$sql} \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        return [];
    }

}

?>