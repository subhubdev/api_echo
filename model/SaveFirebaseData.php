<?php

class FireBaseData {
    public $type;
    private $dbconnect;
    public $logs = array();

    public function __construct($db) {
        $this->dbconnect = $db;  
    }

    public function saveFirebaseDataToSql($firebase_data) {

        $firebase_data = serialize($firebase_data); // json_encode($firebase_data);
        $firebase_data = htmlspecialchars($firebase_data);
        $firebase_data = $this->dbconnect->qstr($firebase_data);
    
        $sqlExists = sprintf("SELECT `type` FROM firebase_homepage_sections WHERE `type` = '%s'", $this->type);
        $res = $this->dbconnect->getRow($sqlExists);
        if ($res) {
            $sql = "UPDATE firebase_homepage_sections SET `firebase_data` = {$firebase_data} WHERE `type` = '{$this->type}'";
        } 
        else {
            $sql = "INSERT INTO firebase_homepage_sections (`firebase_data`, `homepage_type`, `type`) VALUES ({$firebase_data}, '', '{$this->type}')";
        }

        $result = $this->dbconnect->Execute($sql);
        if ($result) {
            return true;
        }
    
        $this->logs[] = __CLASS__."\nSQL: {$sql} \nresult var: $result \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
        return false;
    }

    public function checkContentTypeIfExists($contentType) {

        $sqlExists = "SELECT `firebase_data` FROM firebase_homepage_sections WHERE `type` = ". "'{$contentType}'";
        $res = $this->dbconnect->getRow($sqlExists);
        if ($res) {
            $result = htmlspecialchars_decode($res['firebase_data']);
            $finalresult = unserialize($result);
            return $finalresult;
        }
        return false ;
    }

    public function getFirebaseDataFromMysqlByPageType($contentType) {

        $result = [];
        $sqlExists = "SELECT `firebase_data` FROM firebase_homepage_sections WHERE `type` = ". "'{$contentType}'";
        $res = $this->dbconnect->getAll($sqlExists);
        if ($res) {
            $result = htmlspecialchars_decode($res[0]['firebase_data']);
            $finalresult = unserialize($result);
            return $finalresult;
        }
        return $result;
    }

   // LANDING PAGE

   public function saveLandingPage($firebase_data) {

    $meta_title = $this->dbconnect->qstr($firebase_data['seo_title']);
    $meta_description = $this->dbconnect->qstr($firebase_data['meta_description']);
    $url = $this->dbconnect->qstr($firebase_data['url']);
    $is_live = !$firebase_data['disabled'] ? 1 : 0;
    $slug = $this->dbconnect->qstr($firebase_data['slug']);
    $document_id = $this->dbconnect->qstr($firebase_data['document_id']); // what if they deleted the existing landing page I think we need to truncate the table

    $sqlExists = "SELECT id FROM `seo_meta` WHERE `document_id` = $document_id";
    $res = $this->dbconnect->getRow($sqlExists);
    if ($res) {
        $id = $res['id'];
        $sql = "UPDATE `seo_meta` SET `meta_title` = $meta_title , `meta_description` = $meta_description, `url` = $url, `is_live` = $is_live, `slug` = $slug WHERE `id` = {$id} ";
    } 
    else {
        $sql = "INSERT INTO `seo_meta` (`meta_title` , `meta_description`, `url`, `is_live`,`slug`, `document_id`) VALUES ($meta_title, $meta_description, $url, $is_live, $slug, $document_id) ";
    }

    $result = $this->dbconnect->Execute($sql);
    if ($result) {
        return true;
    }

    $this->logs[] = __CLASS__."\nSQL: {$sql} \nresult var: $result \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
    return false;

}

 public function clearSeoMeta() {

    $sql = "TRUNCATE TABLE `seo_meta`; ";

    $result = $this->dbconnect->Execute($sql);
    if ($result) {
        return true;
    }

    $this->logs[] = __CLASS__."\nSQL: {$sql} \nresult var: $result \n Function name : " . __FUNCTION__ . ' Line number ' . __LINE__ ;
    return false;

 }
}

?>