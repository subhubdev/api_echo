<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Encryption from alveda
 */
class Encryption {

	// ----------------------------------------------------------------------------------
	// Encryption using XMailCrypt algorithm
	// ----------------------------------------------------------------------------------
	private $secretKey = 'd0a6d52a3f5cd11dde2ed6a051bb99b0';
    
	function sys_encrypt($str) {
		$str = $this->data_encrypt($str, $this->secretKey);
		$strEncrypt = "";
		for ($i=0; $i<strlen($str); $i++) {
			$strEncrypt .= substr('00'.dechex((ord(substr($str,$i,1)) ^ 101) & 255), -2);
		}
		
		return strtolower($strEncrypt);
	}
	



	// ----------------------------------------------------------------------------------
	// Decryption using XMailCrypt algorithm
	// ----------------------------------------------------------------------------------
	
	function sys_decrypt($str) {
		$strDecrypt = "";
		for ($i=0; $i<strlen($str); $i=$i+2) {
			$strDecrypt .= chr(hexdec(substr($str,$i,2)) ^ 101);
		}
		
		return $this->data_decrypt($strDecrypt, $this->secretKey);
	}
	



	// ----------------------------------------------------------------------------------
	// Encryption using PC1 Cipher 128-bit
	// ----------------------------------------------------------------------------------
	
	function data_encrypt($text, $key) {
	  	return base64_encode($this->pc1Encrypt($text, $key));
	}
	
	
	

	// ----------------------------------------------------------------------------------
	// Decryption using PC1 Cipher 128-bit
	// ----------------------------------------------------------------------------------
	
	function data_decrypt($text, $key) {
	 	return $this->pc1Decrypt(base64_decode($text), $key);
	}
	

	

	// ==================================================================================================
	// PC1 Cipher 128-bit key
	// (c) Alexander PUKALL 1991
	// Can be use freely even for commercial applications
	//
	// PHP version by: Alvin I. Pineda (alveda@pinoywebsys.com)
	// ==================================================================================================
	
	function pc1_Assemble($pc1_cle, $pc1_x1a2, $pc1_si) {
		$pc1_x1a = (($pc1_cle[0] * 256) + $pc1_cle[1]) % 65536;
		list($pc1_x1a, $pc1_x1a2, $pc1_res, $pc1_si) = $this->pc1_Code(0, $pc1_x1a, $pc1_x1a2, $pc1_si);
		$pc1_inter = $pc1_res;
		
		for ($i=2; $i<15; $i=$i+2) {
			$pc1_x1a = $pc1_x1a ^ (($pc1_cle[$i] * 256) + $pc1_cle[($i+1)]);
			list($pc1_x1a, $pc1_x1a2, $pc1_res, $pc1_si) = $this->pc1_Code(($i/2), $pc1_x1a, $pc1_x1a2, $pc1_si);
			$pc1_inter = $pc1_inter ^ $pc1_res;
		}
		
		return array($pc1_inter, $pc1_x1a2, $pc1_si);
	}

	
	function pc1_Code($pc1_i, $pc1_x1a, $pc1_x1a2, $pc1_si) {
		$pc1_dx = ($pc1_x1a2 + $pc1_i) % 65536;
		$pc1_ax = $pc1_x1a;
		$pc1_cx = 0x15A;
		$pc1_bx = 0x4E35;
		
		$pc1_tmp = $pc1_ax;
		$pc1_ax = $pc1_si;
		$pc1_si = $pc1_tmp;
		
		$pc1_tmp = $pc1_ax;
		$pc1_ax = $pc1_dx;
		$pc1_dx = $pc1_tmp;
		
		if ($pc1_ax) 
			$pc1_ax = ($pc1_ax * $pc1_bx) % 65536;
		
		$pc1_tmp = $pc1_ax;
		$pc1_ax = $pc1_cx;
		$pc1_cx = $pc1_tmp;
		
		if ($pc1_ax) {
			$pc1_ax = ($pc1_ax * $pc1_si) % 65536;
			$pc1_cx = ($pc1_ax + $pc1_cx) % 65536;
		}
		
		$pc1_tmp = $pc1_ax;
		$pc1_ax = $pc1_si;
		$pc1_si = $pc1_tmp;
		$pc1_ax = ($pc1_ax * $pc1_bx) % 65536;
		$pc1_dx = ($pc1_cx + $pc1_dx) % 65536;
		
		$pc1_ax = $pc1_ax + 1;
		
		$pc1_x1a2 = $pc1_dx;
		$pc1_x1a = $pc1_ax;
		
		$pc1_res = $pc1_ax ^ $pc1_dx;
		
		return array($pc1_x1a, $pc1_x1a2, $pc1_res, $pc1_si);
	}
	
	
	function pc1Encrypt($txtString, $txtPassword) {
		$tmpText = "";
		$pc1_si = 0;
		$pc1_x1a2 = 0;
		$pc1_i = 0;
		
		for ($fois=0; $fois<16; $fois++)
			$pc1_cle[$fois] = 0;
		
		$champ1 = $txtPassword;
		$lngchamp1 = strlen($champ1);
		
		for ($fois=0; $fois<$lngchamp1; $fois++)
			$pc1_cle[$fois] = ord(substr($champ1, $fois, 1));
		
		$champ1 = $txtString;
		$lngchamp1 = strlen($champ1);
		for ($fois=0; $fois<$lngchamp1; $fois++) {
			$pci_c = ord(substr($champ1, $fois, 1));
			
			list($pc1_inter, $pc1_x1a2, $pc1_si) = $this->pc1_Assemble($pc1_cle, $pc1_x1a2, $pc1_si);
			
			if ($pc1_inter > 65535)
				$pc1_inter = $pc1_inter - 65536;
			
			$cfc = ((($pc1_inter / 256) * 256) - ($pc1_inter % 256)) / 256;
			$cfd = $pc1_inter % 256;
			
			for ($compte=0; $compte<16; $compte++)
				$pc1_cle[$compte] = $pc1_cle[$compte] ^ $pci_c;
			
			$pci_c = $pci_c ^ ($cfc ^ $cfd);
			
			$d = ((($pci_c / 16) * 16) - ($pci_c % 16)) / 16;
			$e = $pci_c % 16;
			
			$tmpText .= chr(0x61 + $d); 	// d+0x61 give one letter range from a to p for the 4 high bits of $pci_c
			$tmpText .= chr(0x61 + $e); 	// e+0x61 give one letter range from a to p for the 4 low bits of $pci_c
		}
		
		return $tmpText;
	}

	
	function pc1Decrypt($txtString, $txtPassword) {
		$tmpText = "";
		$pc1_si = 0;
		$pc1_x1a2 = 0;
		$pc1_i = 0;
		$pci_c = 0;
		for ($fois=0; $fois<16; $fois++)
			$pc1_cle[$fois] = 0;
		
		$champ1 = $txtPassword;
		$lngchamp1 = strlen($champ1);
		
		for ($fois=0; $fois<$lngchamp1; $fois++)
			$pc1_cle[$fois] = ord(substr($champ1, $fois, 1));
		
		$champ1 = $txtString;
		$lngchamp1 = strlen($champ1);
		
		for ($fois=0; $fois<$lngchamp1; $fois++) {
			$d = ord(substr($champ1, $fois, 1));
			if (($d - 0x61) >= 0) {
				$d = $d - 0x61;  	// to transform the letter to the 4 high bits of $pci_c
		
				if (($d >= 0) && ($d <= 15))
					$d = $d * 16;
			}
		
			if ($fois != $lngchamp1) 
				$fois++;
		
			$e = ord(substr($champ1, $fois, 1));
			if (($e - 0x61) >= 0) {
				$e = $e - 0x61;		// to transform the letter to the 4 low bits of $pci_c
	
				if (($e >= 0) && ($e <= 15))
					$pci_c = $d + $e;
			}
			
			list($pc1_inter, $pc1_x1a2, $pc1_si) = $this->pc1_Assemble($pc1_cle, $pc1_x1a2, $pc1_si);
			
			if ($pc1_inter > 65535)
				$pc1_inter = $pc1_inter - 65536;
			
			$cfc = ((($pc1_inter / 256) * 256) - ($pc1_inter % 256)) / 256;
			$cfd = $pc1_inter % 256;
			
			$pci_c = $pci_c ^ ($cfc ^ $cfd);
			
			for ($compte=0; $compte<16; $compte++)
				$pc1_cle[$compte] = $pc1_cle[$compte] ^ $pci_c;
			
			$tmpText .= chr($pci_c);
		}
		
		return $tmpText;
	}
}
