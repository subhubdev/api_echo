<?php
require_once(NEURON_ROOT. 'includes/js-css-minifier/vendor/autoload.php');
require_once(NEURON_ROOT. 'includes/js-css-minifier/vendor/matthiasmullie/minify/src/CSS.php');
require_once(NEURON_ROOT. 'includes/js-css-minifier/vendor/matthiasmullie/minify/src/JS.php');

use MatthiasMullie\Minify;

class JsCssMinifier {

    public function minifyCss ($sourcePath, $minifiedPath) {
        $minifier = new Minify\CSS($sourcePath);
        $minifier->minify($minifiedPath);
    }

    public function minifyJs ($sourcePath, $minifiedPath ) {
        $sourcePath = NEURON_ROOT. 'js/slider_firebase.js';
        $minifiedPath = NEURON_ROOT. 'js/slider_firebase.min.js';
        $minifier = new Minify\JS($sourcePath);
        $minifier->minify($minifiedPath);
    }


}
?>