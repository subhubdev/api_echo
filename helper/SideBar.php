<?php

function categoryBlockFields() {
    return array (
        "background_color" => "",
        "font-size" => "",
        "links_underline" => "",
        "links_text_color" => "",
        "links_background_color" => ""
    );
}

function loginBlockFields() {
    return array (
        "background_color" => "",
        "button_background_color" => "",
    );
}

function menuBlockFields() {
    return array (
        "background_color" => "",
        "font_size" => "",
        "links_alignment" => "",
        "links_text_color" => "",
        "links_weight" => "",
        "links_underline" => ""
    );
}

function newsLetterBlockFields() {
    return array (
        "background_color" => "",
        "button_color" => "",
        "button_text_color" => ""
    );
}

function profileBlockFields() {
    return array (
        "background_color" => ""
    );
}

function recentArticlesBlockFields() {
    return array (
        "background_color" => "",
        "title_font_size" => "",
        "date_font_size" => "",
        "teaser_font_size" => "",
        "read_more_link_text_color" => "",
        "read_more_link_underline" => "",
        "read_more_font_size" => ""

    );
}

function searchBlockFields() {
    return array (
        "background_color" => "",
        "button_background_color" => ""
    );
}

function eventBlockFields() {
    return array ();
}
