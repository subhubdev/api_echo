<?php

class Variables {

    public function __construct() {
        //Nothing to Declare
    }

    public function googleFontList() {
		return array(
            'url' => '',
            'font_family' => '',
            'generic_font_family' => '',
            'base_size' => '',
            'font_weight' => '',
            'line_height' => '',
            'style' => '',
            'letter_spacing' => '',
            'header_1' => '',
            'header_2' => '',
            'header_3' => '',
            'header_4' => '',
            'header_5' => '',
            'header_6' => ''
        );
    }

    public function webFontList() {
		return array(
            'font_family' => '',
            'base_size' => '',
            'font_weight' => '',
            'line_height' => '',
            'style' => '',
            'letter_spacing' => '',
            'header_1' => '',
            'header_2' => '',
            'header_3' => '',
            'header_4' => '',
            'header_5' => '',
            'header_6' => ''
        );
    }
    
    public function spacingList() {
		return array(
            'section_top_margin' => '',
            'section_bottom_margin' => '',
            'content_top_padding' => '',
            'content_bottom_padding' => '',
            'content_left_padding' => '',
            'content_right_padding' => '',
            'page_width' => ''
        );
    }

    public function codeBlockList() {
		return array(
            '_section_width' => ''
        );
    }
    
    public function colorList() {
		return array(
            'text_links' => '',
            'text_links_hover' => '',
            'text_heading_1' => '',
            'text_heading_2' => '',
            'text_heading_3' => '',
            'text_heading_4' => '',
            'text_heading_5' => '',
            'text_heading_6' => '',
            'article_title' => '',
            'article_title_hover' => '',
            'category_title' => '',
            'category_title_hover' => '',
            'content' => '',
            'buttons_background' => '',
            'buttons_text' => '',
            'buttons_hover' => '',
            'buttons_border' => ''
        );
    }
    
    public function topNavigationList() {
		return array(
            'background_color' => '',
            'nav_links_details' => array (
                'font_color' => '',
                'font_size' => '',
                'font_weight' => ''
            ),
            'logo_details' => array(
                'font_color' => '',
                'font_size' => '',
                'font_weight' => '',
                'logo_set_dimension' => array(
                    'height' => '',
                    'width' => ''
                ),
                'logo_set_dimension_evo' => array(
                    'height' => '',
                    'width' => ''
                )
            ),
            'top_bar' => array(
                'background_color' => '',
                'subscribe_background_color' => ''
            ),
            'border_top' => array(
                'color' => '',
                'style' => '',
                'size' => ''
            ),
            'last_nav_button' => array(
                'button_font_size' => '',
                'button_border_radius' => '',
                'button_border' => '',
                'hover_button' => array(
                    'background_color' => '',
                    'button_text_color' => '',
                    'button_border_color' => '',
                ),
                'normal_button' => array(
                    'background_color' => '',
                    'button_text_color' => '',
                    'button_border_color' => '',
                ),
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'button_style' => '' // Needed for checking Old Data
            )
        );
            
    }

    public function textSectionList() {
		return array(
            'background_color' => '',
            '_hr_tag' => array (
                'background' => '',
                'width' => '',
                'height' => ''
            )
        );      
    }

    public function spacerList() {
		return array(
            'background_color' => '',
            'horizontal_rule_color' => '',
            'height' => ''
        ); 
    }

    public function showcaseList() {
		return array(
            'background_color' => '',
            'showcase_column_1' => array (
                'background_color' => '',
                'button_border_color' => '',
                'button_border_width' => '',
                'button_background' => '',
                'button_label_color' => '',
                'button_width' => '',
                'button_hover_background' => '',
                'button_hover_border_color' => '',
                'button_hover_label_color' => '',
                'button_font_size' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'background_type' => '',
                'button_border_radius' => ''
            ),
            'showcase_column_2' => array (
                'background_color' => '',
                'button_border_color' => '',
                'button_border_width' => '',
                'button_background' => '',
                'button_label_color' => '',
                'button_width' => '',
                'button_hover_background' => '',
                'button_hover_border_color' => '',
                'button_hover_label_color' => '',
                'button_font_size' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'background_type' => '',
                'button_border_radius' => ''
            ),
            'showcase_column_3' => array (
                'background_color' => '',
                'button_border_color' => '',
                'button_border_width' => '',
                'button_background' => '',
                'button_label_color' => '',
                'button_width' => '',
                'button_hover_background' => '',
                'button_hover_border_color' => '',
                'button_hover_label_color' => '',
                'button_font_size' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'background_type' => '',
                'button_border_radius' => ''
            ),
            'showcase_column_4' => array (
                'background_color' => '',
                'button_border_color' => '',
                'button_border_width' => '',
                'button_background' => '',
                'button_label_color' => '',
                'button_width' => '',
                'button_hover_background' => '',
                'button_hover_border_color' => '',
                'button_hover_label_color' => '',
                'button_font_size' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'background_type' => '',
                'button_border_radius' => ''
            ),
            'showcase_column_5' => array (
                'background_color' => '',
                'button_border_color' => '',
                'button_border_width' => '',
                'button_background' => '',
                'button_label_color' => '',
                'button_width' => '',
                'button_hover_background_color' => '',
                'button_hover_border_color' => '',
                'button_hover_label_color' => '',
                'button_font_size' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'background_type' => '',
                'button_border_radius' => ''
            )
        ); 
    }

    public function footerList() {
		return array(
            'social' => array(
                'icon_background_color' => '',
                'icon_color' => ''
            ),
            'custom' => array(
                'horizontal_rule_color' => '',
                'vertical_rule_color' => '',
                'social_icon_border' => ''
            ),
            'appearance' => array(
                'text_color' => '',
                'background_color' => ''
            ),
            'logo_details' => array(
                'logo_set_dimension' => array(
                    'height' => '',
                    'width' => ''
                )
            )            
        );      
    }

    public function callToActionList() {
        return array(
            'button_font_size' => '',
            'button_border_radius' => '',
            'button_border' => '', // Also known as button_border_width
            'hover_button' => array (
                'background_color' => '',
                'button_border_color' => '',
                'button_text_color' => ''
            ),
            'normal_button' => array (
                'background_color' => '',
                'button_text_color' => '',
                'button_border_color' => ''
            ),
            'button_padding_top' => '',
            'button_padding_left' => '',
            'button_padding_right' => '',
            'button_padding_bottom' => '',
            'button_style' => '' // Needed for checking Old Data
        );
    }

    public function articleList() {
        return array(
            'show_article_thumbnail' => ''
        );
    }

    public function featuredCoursesList() {
        return array(
            
        );
    }

    public function articlesCategoryList() {
        return array(
            'image_overlay_color' => '',
            'image_overlay_opacity' => ''
        );
    }

    public function textImageList() {
		return array(
            'image_position' => '',
            'background_color' => '',
            'text_column_left_padding' => '',
            'text_column_right_padding' => '',
            'button' => array (
                'button_font_size' => '',
                'button_border_radius' => '',
                'button_border_width' => '',
                'button_background_color' => '',
                'button_border_color' => '',
                'button_text_color' => '',
                'button_hover_background_color' => '',
                'button_hover_border_color' => '',
                'button_hover_text_color' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => ''
            )
        ); 
    }

    public function textImageFullList() {
		return array(
            'image_position' => '',
            'background_color' => '',
            'image_column_width' => '',
            'text_column_left_padding' => '',
            'text_column_right_padding' => '',
            'button' => array (
                'button_font_size' => '',
                'button_border_radius' => '',
                'button_border_width' => '',
                'button_background_color' => '',
                'button_border_color' => '',
                'button_text_color' => '',
                'button_hover_background_color' => '',
                'button_hover_border_color' => '',
                'button_hover_text_color' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => ''
            )
        ); 
    }

    public function eventsList() {
		return array(); 
    }

    public function faqList() {
		return array(
            'icon_color' => '',
            'section_background_color' => '',
            'section_top_padding' => '',
            'section_bottom_padding' => '',
            'section_left_padding' => '',
            'section_right_padding' => '',
            'border_color' => '',
            'border_width' => '',
            'question_background_color' => '',
            'question_padding_top' => '',
            'question_padding_bottom' => '',
            'question_padding_left' => '',
            'question_padding_right' => '',
            'answer_background_color' => '',
            'answer_padding_top' => '',
            'answer_padding_bottom' => '',
            'answer_padding_left' => '',
            'answer_padding_right' => ''
        ); 
    }

    public function textVideoFullList() {
		return array(
            'video_position' => '',
            'background_color' => '',
            'video_column_width' => '',
            'text_column_left_padding' => '',
            'text_column_right_padding' => '',
            'button' => array (
                'button_font_size' => '',
                'button_border_radius' => '',
                'button_border_width' => '',
                'button_background_color' => '',
                'button_border_color' => '',
                'button_text_color' => '',
                'button_hover_background_color' => '',
                'button_hover_border_color' => '',
                'button_hover_text_color' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => ''
            )
        ); 
    }

    public function testimonialSliderFields() {
        return array (
            "section_title_top_padding" => "",
            "section_title_bottom_padding" => "",
            "section_title_left_padding" => "",
            "section_title_right_padding" => "",
            "section_title_background_color" => "",

            "content_config" => array (
                "content_vertical_position" => "",
                "content_horizontal_position" => ""
            ),
            "testimonial_config" => array(
                "testimonial_padding" => "",
                "testimonial_content_width" => "",
                "testimonial_custom_height" => "",
                "testimonial_height" => ""
            ),
            "button_config" => array(
                'button_font_size' => '',
                'button_border_radius' => '',
                'button_border' => '', // Also known as button_border_width
                'button_background_color' => '',
                'button_border_color' => '',
                'button_text_color' => '',
                'button_hover_color' => '',
                'button_hover_border_color' => '',
                'button_hover_text_color' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => '',
                'button_size' => '', // I will need the value of this to implement the custom Padding Applied to all exisitn data
                'button_style' => '' // Needed for checking Old Data
            ),
            
            "_testimonials" => array(
                "_testimonial_1" => ""
            ),
            "_testimonial_1" => $this->testimonialSlideFields() ,
            "_testimonial_2" => $this->testimonialSlideFields() ,
            "_testimonial_3" => $this->testimonialSlideFields() ,
            "_testimonial_4" => $this->testimonialSlideFields() ,
            "_testimonial_5" => $this->testimonialSlideFields() ,
            "_testimonial_6" => $this->testimonialSlideFields() ,
            "_testimonial_7" => $this->testimonialSlideFields() ,
            "_testimonial_8" => $this->testimonialSlideFields() ,
            "_testimonial_9" => $this->testimonialSlideFields() ,
            "_testimonial_10" => $this->testimonialSlideFields() ,
            "_testimonial_11" => $this->testimonialSlideFields() ,
            "_testimonial_12" => $this->testimonialSlideFields() ,
            "_testimonial_13" => $this->testimonialSlideFields() ,
            "_testimonial_14" => $this->testimonialSlideFields() ,
            "_testimonial_15" => $this->testimonialSlideFields() ,
            "_testimonial_16" => $this->testimonialSlideFields() ,
            "_testimonial_17" => $this->testimonialSlideFields() ,
            "_testimonial_18" => $this->testimonialSlideFields() ,
            "_testimonial_19" => $this->testimonialSlideFields() ,
            "_testimonial_20" => $this->testimonialSlideFields() ,
            "_testimonial_21" => $this->testimonialSlideFields() ,
            "_testimonial_22" => $this->testimonialSlideFields() ,
            "_testimonial_23" => $this->testimonialSlideFields() ,
            "_testimonial_24" => $this->testimonialSlideFields() ,
            "_testimonial_25" => $this->testimonialSlideFields() ,
            "_testimonial_26" => $this->testimonialSlideFields() ,
            "_testimonial_27" => $this->testimonialSlideFields() ,
            "_testimonial_28" => $this->testimonialSlideFields() ,
            "_testimonial_29" => $this->testimonialSlideFields() ,
            "_testimonial_30" => $this->testimonialSlideFields() ,
            "navigation_config" => array(
                "navigation_show_arrows" => "",
                "navigation_arrow_size" => "",
                "navigation_arrow_colors" => ""
            ),
    
        );
    }

    public function testimonialSlideFields() {
        return array(
            "id" => "",
            "opacity_overlay_color" => "",
            "image" => array(
                "dimension" => array(
                    "height" => "",
                    "width" => ""
                ),
            ),
            "button_size" => "",
            "image_overlay_color" => "",
            "top_spacing" => "",
            "bottom_spacing" => "",
            "button_style" => "",                
            "button_color" => "",
            "horizontal_bar_width" => "",
            "right_spacing" => "",
            "alignment" => "",
            "header_text_color" => "",
            "spacing" => "",
            "left_spacing" => "",
            "button_width" => "",
            "horizontal_bar_height" => "",
            "show_button" => "",
            "button_text_color" => "",
            "testimonial_type" => "",
            "background_color" => ""
        );
    }

    public function newsletterFields() {
		return array(
            'gradient_background_color_one' => '',
            'gradient_background_color_two' => '',
            'button' => array (
                'button_font_size' => '',
                'button_border_radius' => '',
                'button_border_width' => '',
                'button_background_color' => '',
                'button_border_color' => '',
                'button_text_color' => '',
                'button_hover_background_color' => '',
                'button_hover_border_color' => '',
                'button_hover_text_color' => '',
                'button_padding_top' => '',
                'button_padding_left' => '',
                'button_padding_right' => '',
                'button_padding_bottom' => ''
            ),
            'formfield' => array (
                'form_field_font_size' => '',
                'form_field_border_radius' => '',
                'form_field_border_width' => '',
                'form_field_background_color' => '',
                'form_field_border_color' => '',
                'form_field_text_color' => '',
                'form_field_padding_top' => '',
                'form_field_padding_left' => '',
                'form_field_padding_right' => '',
                'form_field_padding_bottom' => ''
            )
        ); 
    }
}

?>