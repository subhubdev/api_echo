<?php

function bannerFields() {
    return array (
        "background_type" =>"",
        "banner_image_source" => "",
        "_section_width" => "",
        "_section_width" => "",

        "button_font_size" => "",
        "button_border_radius" => "",
        "button_border" => "",
        "button_color" => "", // Also known as button_background_color
        "button_border_color" => "",
        "button_text_color" => "",
        "button_hover_color" => "",
        "button_hover_border_color" => "",
        "button_hover_text_color" => "",
        "button_padding_top" => "",
        "button_padding_left" => "",
        "button_padding_right" => "",
        "button_padding_bottom" => "",
        "button_size" => "", // I will need the value of this to implement the custom Padding Applied to all exisitn data
        "button_style" => "", // Needed for checking Old Data

        "opacity_overlay_color" => "",
        "color" => array (
            "header_content_border_width" => "",
            "header_content_background_color" => "",
            "text_content_background_color" => "",
            "background_color" => ""
        ),
        "image_overlay_color" => "",
        "layout" => array (
            "content_vertical_position" => "",
            "banner_custom_height" => "",
            "banner_padding" => "",
            "banner_height" => "",
            "banner_content_width" => "",
            "content_horizontal_position" => ""
        ),
        "top_spacing" => "",
        "bottom_spacing" => "",
        "hr_tag" => array(
            "height" => "",
            "background_color" => "",
            "width" => ""
        ),
        
        "right_spacing" => "",
        "alignment" => "",
        "spacing" => "" ,
        "left_spacing" => "",
        "vertical_middle_position" => "",
        "content" => array(
            "image_text_content" => array(
                "dimension" => array(
                    "width" => "",
                    "height" => ""
                ),

            )
        )
    );

  }

function colorSchemePosition($homepage_sections) {
    
    $bannerHeight = $homepage_sections['layout']['banner_height'];
    $customHeight = $homepage_sections['layout']['banner_custom_height'];
    $textContainerHeight = 0;
    $verticalPosition = $homepage_sections['layout']['content_vertical_position'];
    $position = ($customHeight / 2) - ($textContainerHeight / 2);

    if ($bannerHeight === 'custom' && $verticalPosition === 'middle') {
        $homepage_sections['vertical_middle_position'] = $position.'px';
    } 
    else {
        $homepage_sections['vertical_middle_position'] = '0px';
    }

    return $homepage_sections;
}

function sliderFields() {
    return array (
        "content_config" => array (
            "content_vertical_position" => "",
            "content_horizontal_position" => ""
        ),
        "slider_config" => array(
            "slider_padding" => "",
            "slider_content_width" => "",
            "slider_custom_height" => "",
            "slider_height" => ""
        ),
        "button_config" => array(
            'button_font_size' => '',
            'button_border_radius' => '',
            'button_border' => '', // Also known as button_border_width
            'button_background_color' => '',
            'button_border_color' => '',
            'button_text_color' => '',
            'button_hover_color' => '',
            'button_hover_border_color' => '',
            'button_hover_text_color' => '',
            'button_padding_top' => '',
            'button_padding_left' => '',
            'button_padding_right' => '',
            'button_padding_bottom' => '',
            'button_size' => '', // I will need the value of this to implement the custom Padding Applied to all exisitn data
            'button_style' => '' // Needed for checking Old Data
        ),
        
        "_slides" => array(
            "_slide_1" => ""
        ),
        "_slide_1" => slideFields() ,
        "_slide_2" => slideFields() ,
        "_slide_3" => slideFields() ,
        "_slide_4" => slideFields() ,
        "_slide_5" => slideFields() ,
        "_slide_6" => slideFields() ,
        "_slide_7" => slideFields() ,
        "_slide_8" => slideFields() ,
        "_slide_9" => slideFields() ,
        "_slide_10" => slideFields() ,
        "_slide_11" => slideFields() ,
        "_slide_12" => slideFields() ,
        "_slide_13" => slideFields() ,
        "_slide_14" => slideFields() ,
        "_slide_15" => slideFields() ,
        "_slide_16" => slideFields() ,
        "_slide_17" => slideFields() ,
        "_slide_18" => slideFields() ,
        "_slide_19" => slideFields() ,
        "_slide_20" => slideFields() ,
        "_slide_21" => slideFields() ,
        "_slide_22" => slideFields() ,
        "_slide_23" => slideFields() ,
        "_slide_24" => slideFields() ,
        "_slide_25" => slideFields() ,
        "_slide_26" => slideFields() ,
        "_slide_27" => slideFields() ,
        "_slide_28" => slideFields() ,
        "_slide_29" => slideFields() ,
        "_slide_30" => slideFields() ,
        "navigation_config" => array(
            "navigation_show_arrows" => "",
            "navigation_arrow_size" => "",
            "navigation_arrow_colors" => ""
        ),

    );
}

    function slideFields() {
        return array(
            "id" => "",
            "opacity_overlay_color" => "",
            "image" => array(
                "dimension" => array(
                    "height" => "",
                    "width" => ""
                ),
            ),
            "background_color" => "",
            "button_size" => "",
            "image_overlay_color" => "",
            "top_spacing" => "",
            "bottom_spacing" => "",
            "button_style" => "",                
            "button_color" => "",
            "horizontal_bar_width" => "",
            "right_spacing" => "",
            "alignment" => "",
            "header_text_color" => "",
            "spacing" => "",
            "left_spacing" => "",
            "button_width" => "",
            "horizontal_bar_height" => "",
            "show_button" => "",
            "button_text_color" => "",
            "slider_type" => ""
        );
    }